# Spanish translations for docs_krita_org_reference_manual___preferences___g_mic_settings.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___preferences___g_mic_settings\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-02-19 19:25+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.2\n"

#: ../../reference_manual/preferences/g_mic_settings.rst:1
msgid "How to setup G'Mic in Krita."
msgstr "Cómo configurar G'Mic en Krita."

#: ../../reference_manual/preferences/g_mic_settings.rst:10
msgid "Preferences"
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:10
#, fuzzy
#| msgid "G'Mic Settings"
msgid "Settings"
msgstr "Preferencias de G'Mic"

#: ../../reference_manual/preferences/g_mic_settings.rst:10
msgid "Filters"
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:10
msgid "G'Mic"
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:15
msgid "G'Mic Settings"
msgstr "Preferencias de G'Mic"

#: ../../reference_manual/preferences/g_mic_settings.rst:17
msgid ""
"G'Mic or GREYC's Magic for Image Computing is an opensource filter "
"framework, or, it is an extra program you can download to have access to a "
"whole lot of image filters."
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:19
msgid ""
"Krita has had G'Mic integration for a long time, but this is its most stable "
"incarnation."
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:21
msgid "You set it up as following:"
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:23
msgid ""
"First download the proper krita plugin from `the G'Mic website. <https://"
"gmic.eu/download.shtml>`_."
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:24
msgid "Then, unzip and place it somewhere you can find it."
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:25
msgid ""
"Go to :menuselection:`Settings --> Configure Krita --> G'Mic plugin` and set "
"G'MIC to the filepath there."
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:26
msgid "Then restart Krita."
msgstr "A continuación, reinicie Krita."

#: ../../reference_manual/preferences/g_mic_settings.rst:30
msgid "Updates to G'Mic"
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:32
msgid ""
"There is a refresh button at the bottom of the G'Mic window that will update "
"your version. You will need an internet connection to download the latest "
"version."
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:34
msgid ""
"If you have issues downloading the update through the plugin, you can also "
"do it manually. If you are trying to update and get an error, copy the URL "
"that is displayed in the error dialog. It will be to a \".gmic\" file. "
"Download it from from your web browser and place the file in one of the "
"following directories."
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:36
msgid "Windows : %APPDATA%/gmic/update2XX.gmic"
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:37
msgid "Linux : $HOME/.config/gmic/update2XX.gmic"
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:39
msgid ""
"Load up the G'Mic plugin and press the refresh button for the version to "
"update."
msgstr ""
