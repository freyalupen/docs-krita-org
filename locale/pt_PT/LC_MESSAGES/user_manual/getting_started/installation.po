# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:18+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-IgnoreConsistency: Arch\n"
"X-POFile-SpellExtra: wiki Fedora krita aurman chmod LXDE AppImage Yast\n"
"X-POFile-SpellExtra: Snap OSX dnf Kubuntu calligra OpenSUSE Mac aur Ubuntu\n"
"X-POFile-SpellExtra: Appimages Arch channel Gnome Molch Nautilus apt KRA\n"
"X-POFile-SpellExtra: bash exe XCF PackageKit appimage setup Discover\n"
"X-POFile-SpellExtra: Moritz Krita SO install Revoy kra pacman PPA EXE\n"
"X-POFile-IgnoreConsistency: Windows\n"
"X-POFile-SpellExtra: pkgbuild guilabel sum menuselection Xfce ref Steam\n"

#: ../../user_manual/getting_started/installation.rst:1
msgid "Detailed steps on how to install Krita"
msgstr "Passos detalhados sobre como instalar o Krita"

#: ../../user_manual/getting_started/installation.rst:14
#: ../../user_manual/getting_started/installation.rst:18
msgid "Installation"
msgstr "Instalação"

#: ../../user_manual/getting_started/installation.rst:21
msgid "Windows"
msgstr "Windows"

#: ../../user_manual/getting_started/installation.rst:22
msgid ""
"Windows users can download Krita from the website, the Windows Store, or "
"Steam."
msgstr ""
"Os utilizadores do Windows podem obter o Krita a partir da página Web, da "
"Loja do Windows ou do Steam."

#: ../../user_manual/getting_started/installation.rst:24
msgid ""
"The versions on the Store and Steam cost money, but are `functionally "
"identical <https://krita.org/en/item/krita-available-from-the-windows-store/"
">`_ to the (free) website version. Unlike the website version, however, both "
"paid versions get automatic updates when new versions of Krita comes out. "
"After deduction of the Store fee, the purchase cost supports Krita "
"development."
msgstr ""
"As versões na Loja e no Steam custam dinheiro, mas são `funcionalmente "
"idênticas <https://krita.org/en/item/krita-available-from-the-windows-store/"
">`_ à versão (gratuita) na página Web. Contudo, ambas as versões pagas "
"recebem actualizações automáticas quando surgem novas versões do Krita. "
"Depois da dedução do valor da Loja, o custo da compra ajuda no "
"desenvolvimento do Krita."

#: ../../user_manual/getting_started/installation.rst:31
msgid ""
"The latest version is always on our `website <https://krita.org/download/>`_."
msgstr ""
"A última versão está sempre disponível na nossa `página Web <https://krita."
"org/download/>`_."

#: ../../user_manual/getting_started/installation.rst:33
msgid ""
"The page will try to automatically recommend the correct architecture (64- "
"or 32-bit), but you can select \"All Download Versions\" to get more "
"choices. To determine your computer architecture manually, go to :"
"menuselection:`Settings --> About`. Your architecture will be listed as the :"
"guilabel:`System Type` in the :guilabel:`Device Specifications` section."
msgstr ""
"A página irá tentar recomendar automaticamente a arquitectura correcta (64 "
"ou 32 bits), mas poderá seleccionar \"Todas as Versões para Transferência\" "
"para obter mais opções. Para determinar manualmente a arquitectura do seu "
"computador, vá a :menuselection:`Configuração --> Acerca`. A sua "
"arquitectura irá aparecer no :guilabel:`Tipo de Sistema` na secção :guilabel:"
"`Especificações do Dispositivo`."

#: ../../user_manual/getting_started/installation.rst:35
msgid ""
"Krita by default downloads an **installer EXE**, but you can also download a "
"**portable zip-file** version instead. Unlike the installer version, this "
"portable version does not show previews in Windows Explorer automatically. "
"To get these previews with the portable version, also install Krita's "
"**Windows Shell Extension** extension (available on the download page)."
msgstr ""
"O Krita por omissão irá transferir um **EXE de instalação**, mas também "
"poderá obter um **ficheiro ZIP portátil** em alternativa. Ao contrário da "
"versão de instalação, esta versão portátil não mostra as antevisões "
"automaticamente no Explorador do Windows. Para obter essas antevisões com a "
"versão portátil, instale também a extensão **Extensão da Linha de Comandos "
"do Windows** (disponível na página de transferências)."

#: ../../user_manual/getting_started/installation.rst:36
msgid "Website:"
msgstr "Página Web:"

#: ../../user_manual/getting_started/installation.rst:37
msgid ""
"These files are also available from the `KDE download directory <https://"
"download.kde.org/stable/krita/>`_."
msgstr ""
"Estes ficheiros também estão disponíveis na `pasta de transferências do KDE "
"<https://download.kde.org/stable/krita/>`_."

#: ../../user_manual/getting_started/installation.rst:38
msgid "Windows Store:"
msgstr "Loja do Windows:"

#: ../../user_manual/getting_started/installation.rst:39
msgid ""
"For a small fee, you can download Krita `from the Windows Store <https://www."
"microsoft.com/store/productId/9N6X57ZGRW96>`_. This version requires Windows "
"10."
msgstr ""
"Por um montante baixo, poderá obter o Krita `a partir da Loja do Windows "
"<https://www.microsoft.com/store/productId/9N6X57ZGRW96>`_. Esta versão "
"necessita do Windows 10."

#: ../../user_manual/getting_started/installation.rst:41
msgid ""
"For a small fee, you can also download Krita `from Steam <https://store."
"steampowered.com/app/280680/Krita/>`_."
msgstr ""
"Também por um pequeno montante, também poderá obter o Krita no `Steam "
"<https://store.steampowered.com/app/280680/Krita/>`_."

#: ../../user_manual/getting_started/installation.rst:42
msgid "Steam:"
msgstr "Steam:"

#: ../../user_manual/getting_started/installation.rst:44
msgid ""
"To download a portable version of Krita go to the `KDE <https://download.kde."
"org/stable/krita/>`_ download directory and get the zip-file instead of the "
"setup.exe installer."
msgstr ""
"Para obter uma versão portátil do Krita, vá à pasta de transferências do "
"`KDE <https://download.kde.org/stable/krita/>`_ para obter o pacote ZIP "
"portátil do Krita, em vez do instalador 'setup.exe'."

#: ../../user_manual/getting_started/installation.rst:48
msgid ""
"Krita requires Windows 7 or newer. The Store version requires Windows 10."
msgstr ""
"O Krita precisa do Windows 7 ou posterior. A versão da Loja necessita do "
"Windows 10."

#: ../../user_manual/getting_started/installation.rst:51
msgid "Linux"
msgstr "Linux"

#: ../../user_manual/getting_started/installation.rst:53
msgid ""
"Many Linux distributions package the latest version of Krita. Sometimes you "
"will have to enable an extra repository. Krita runs fine under most desktop "
"enviroments such as KDE, Gnome, LXDE, Xfce etc. -- even though it is a KDE "
"application and needs the KDE libraries. You might also want to install the "
"KDE system settings module and tweak the gui theme and fonts used, depending "
"on your distributions"
msgstr ""
"Muitas distribuições de Linux já têm pacotes para a última versão do Krita. "
"Algumas vezes poderá ter de activar um repositório extra. O Krita funciona "
"perfeitamente em qualquer ambiente de trabalho: KDE, Gnome, LXDE, Xfce, etc. "
"-- mesmo sendo uma aplicação do KDE e necessitando das bibliotecas do KDE. "
"Poderá também querer instalar o módulo de configuração do sistema do KDE e "
"ajustar o tema e tipos de letra usados, dependendo das suas distribuições."

#: ../../user_manual/getting_started/installation.rst:61
msgid "Nautilus/Nemo file extensions"
msgstr "Extensões de ficheiros do Nautilus/Nemo"

#: ../../user_manual/getting_started/installation.rst:63
msgid ""
"Since April 2016, KDE's Dolphin file manager shows kra and ora thumbnails by "
"default, but Nautilus and it's derivatives need an extension. `We recommend "
"Moritz Molch's extensions for XCF, KRA, ORA and PSD thumbnails <https://"
"moritzmolch.com/1749>`__."
msgstr ""
"Desde Abril de 2016, o gestor de ficheiros Dolphin do KDE mostra as "
"miniaturas do .kra e do .ora por omissão, mas o Nautilus e os seus derivados "
"precisam de uma extensão. `Recomendamos as extensões do Moritz Molch para as "
"miniaturas em XCF, KRA, ORA e PSD <https://moritzmolch.com/1749>`__."

#: ../../user_manual/getting_started/installation.rst:69
msgid "Appimages"
msgstr "Appimages"

#: ../../user_manual/getting_started/installation.rst:71
msgid ""
"For Krita 3.0 and later, first try out the appimage from the website. **90% "
"of the time this is by far the easiest way to get the latest Krita.** Just "
"download the appimage, and then use the file properties or the bash command "
"chmod to make the appimage executable. Double click it, and enjoy Krita. (Or "
"run it in the terminal with ./appimagename.appimage)"
msgstr ""
"Para o Krita 3.0 e posteriores, tente primeiro a AppImage da página Web em "
"primeiro lugar. **90% das vezes esta é de longe a forma mais simples de "
"obter o último Krita.** Basta obter a AppImage e depois usar as propriedades "
"de ficheiros ou a linha de comandos do 'bash' com o 'chmod' para tornar a "
"AppImage executável. Faça duplo-click sobre ela e desfrute do Krita. (Ou "
"execute-o no terminal com o comando ./nome-appimage.appimage)"

#: ../../user_manual/getting_started/installation.rst:78
msgid "Open the terminal into the folder you have the appimage."
msgstr "Abra o terminal na pasta onde tem o ficheiro 'appimage'."

#: ../../user_manual/getting_started/installation.rst:79
msgid "Make it executable:"
msgstr "Torne-o executável:"

#: ../../user_manual/getting_started/installation.rst:83
msgid "chmod a+x krita-3.0-x86_64.appimage"
msgstr "chmod a+x krita-3.0-x86_64.appimage"

#: ../../user_manual/getting_started/installation.rst:85
msgid "Run Krita!"
msgstr "Execute o Krita!"

#: ../../user_manual/getting_started/installation.rst:89
msgid "./krita-3.0-x86_64.appimage"
msgstr "./krita-3.0-x86_64.appimage"

#: ../../user_manual/getting_started/installation.rst:91
msgid ""
"Appimages are ISOs with all the necessary libraries bundled inside, that "
"means no fiddling with repositories and dependencies, at the cost of a "
"slight bit more diskspace taken up (And this size would only be bigger if "
"you were using Plasma to begin with)."
msgstr ""
"As Appimages são ISO's com todas as bibliotecas necessárias, o que significa "
"que que não existem interferências com os repositórios e dependências, em "
"detrimento da ocupação de um bocado mais de espaço em disco (Sendo que este "
"ficheiro só seria maior se estivesse a usar o Plasma, em primeiro lugar)."

#: ../../user_manual/getting_started/installation.rst:97
msgid "Ubuntu and Kubuntu"
msgstr "Ubuntu e Kubuntu"

#: ../../user_manual/getting_started/installation.rst:99
msgid ""
"It does not matter which version of Ubuntu you use, Krita will run just "
"fine. However, by default, only a very old version of Krita is available. "
"You should either use the appimage, flatpak or the snap available from "
"Ubuntu's app store. We also maintain a ppa for getting latest builds of "
"Krita, you can read more about the ppa and install instructions `here "
"<https://launchpad.net/~kritalime/+archive/ubuntu/ppa>`_."
msgstr ""
"Não interessa qual a versão do Ubuntu que use; o Krita irá funcionar bem em "
"ambas. Contudo, por omissão, só está disponível uma versão muito antiga do "
"Krita. Deverá usar a AppImage ou o pacote Snap disponível na loja de "
"aplicações da Ubuntu. Também mantemos sum PPA para obter as últimas versões "
"do Krita; poderá ler mais sobre o PPA e as instruções de instalação `aqui "
"<https://launchpad.net/~kritalime/+archive/ubuntu/ppa>`_."

#: ../../user_manual/getting_started/installation.rst:106
msgid "OpenSUSE"
msgstr "OpenSUSE"

#: ../../user_manual/getting_started/installation.rst:108
msgid "The latest stable builds are available from KDE:Extra repo:"
msgstr ""
"As últimas versões estáveis estão disponíveis no repositório KDE:Extra:"

#: ../../user_manual/getting_started/installation.rst:110
msgid "https://download.opensuse.org/repositories/KDE:/Extra/"
msgstr "https://download.opensuse.org/repositories/KDE:/Extra/"

#: ../../user_manual/getting_started/installation.rst:113
msgid "Krita is also in the official repos, you can install it from Yast."
msgstr ""
"O Krita também existe nos repositórios oficiais; podê-lo-á instalar a partir "
"do Yast."

#: ../../user_manual/getting_started/installation.rst:116
msgid "Fedora"
msgstr "Fedora"

#: ../../user_manual/getting_started/installation.rst:118
msgid ""
"Krita is in the official repos, you can install it by using packagekit (Add/"
"Remove Software) or by writing the following command in terminal."
msgstr ""
"O Krita consta nos repositórios oficiais; podê-lo-á instalar se usar o "
"PackageKit (Adicionar/Remover Aplicações) ou se invocar o seguinte comando "
"no terminal."

#: ../../user_manual/getting_started/installation.rst:120
msgid "``dnf install krita``"
msgstr "``dnf install krita``"

#: ../../user_manual/getting_started/installation.rst:122
msgid ""
"You can also use the software center such as gnome software center or "
"Discover to install Krita."
msgstr ""
"Também poderá usar um centro de aplicações, como o Centro de Aplicações do "
"Gnome ou o Discover para instalar o Krita."

#: ../../user_manual/getting_started/installation.rst:125
msgid "Debian"
msgstr "Debian"

#: ../../user_manual/getting_started/installation.rst:127
msgid ""
"The latest version of Krita available in Debian is 3.1.1. To install Krita "
"type the following line in terminal:"
msgstr ""
"A última versão do Krita disponível no Debian é a 3.1.1. Para instalar o "
"Krita, escreva a seguinte linha no terminal:"

#: ../../user_manual/getting_started/installation.rst:130
msgid "``apt install krita``"
msgstr "``apt install krita``"

#: ../../user_manual/getting_started/installation.rst:134
msgid "Arch"
msgstr "Arch"

#: ../../user_manual/getting_started/installation.rst:136
msgid ""
"Arch Linux provides krita package in the Extra repository. You can install "
"Krita by using the following command:"
msgstr ""
"O Arch Linux oferece o pacote do Krita no repositório Extra. Poderá instalar "
"o Krita se usar o seguinte comando:"

#: ../../user_manual/getting_started/installation.rst:139
msgid "``pacman -S krita``"
msgstr "``pacman -S krita``"

#: ../../user_manual/getting_started/installation.rst:141
msgid ""
"You can also find Krita pkgbuild in arch user repositories but it is not "
"guaranteed to contain the latest git version."
msgstr ""
"Também poderá encontrar o 'pkgbuild' do Krita nos repositórios de "
"utilizadores do Arch, mas não se garante que tenha a última versão no Git."

#: ../../user_manual/getting_started/installation.rst:145
msgid "OS X"
msgstr "OS X"

#: ../../user_manual/getting_started/installation.rst:147
msgid ""
"You can download the latest binary from our `website <https://krita.org/"
"download/krita-desktop/>`__. The binaries work only with Mac OSX version "
"10.12 and newer."
msgstr ""
"Poderá obter o último binário, se o desejar, a partir da nossa `página Web "
"<https://krita.org/download/krita-desktop/>`__. O binário só funciona com o "
"Mac OSX 10.12 e posteriores."

#: ../../user_manual/getting_started/installation.rst:152
msgid "Source"
msgstr "Código"

#: ../../user_manual/getting_started/installation.rst:154
msgid ""
"While it is certainly more difficult to compile Krita from source than it is "
"to install from prebuilt packages, there are certain advantages that might "
"make the effort worth it:"
msgstr ""
"Embora seja certamente mais difícil de compilar o Krita a partir do código "
"do que é instalá-lo a partir de pacotes pré-compilados, existem certas "
"vantagens que poderão fazer com que isso valha a pena:"

#: ../../user_manual/getting_started/installation.rst:158
msgid ""
"You can follow the development of Krita on the foot. If you compile Krita "
"regularly from the development repository, you will be able to play with all "
"the new features that the developers are working on."
msgstr ""
"Poderá seguir o desenvolvimento do Krita praticamente em cima da hora. Se "
"compilar o Krita regulamente a partir do repositório de desenvolvimento, "
"poderá brincar com todas as novas funcionalidades com que os programadores "
"estão a trabalhar."

#: ../../user_manual/getting_started/installation.rst:161
msgid ""
"You can compile it optimized for your processor. Most pre-built packages are "
"built for the lowest-common denominator."
msgstr ""
"Poderá compilar o código de forma optimizada para o seu processador. A "
"maioria dos pacotes são compilados com o mínimo denominador comum."

#: ../../user_manual/getting_started/installation.rst:163
msgid "You will be getting all the bug fixes as soon as possible as well."
msgstr "Irá obter todas as correcções de erros assim que possível."

#: ../../user_manual/getting_started/installation.rst:164
msgid ""
"You can help the developers by giving us your feedback on features as they "
"are being developed and you can test bug fixes for us. This is hugely "
"important, which is why our regular testers get their name in the about box "
"just like developers."
msgstr ""
"Poderá ajudar os programadores se nos der as suas opiniões sobre "
"funcionalidades, à medida que ambas vão sendo desenvolvidas, podendo testar "
"as correcções de erros por nós. Isto é bastante importante, e é por isso que "
"os nossos responsáveis regulares pelos testes têm o seu nome no campo "
"'Acerca', tal como os programadores."

#: ../../user_manual/getting_started/installation.rst:169
msgid ""
"Of course, there are also some disadvantages: when building from the current "
"development source repository you also get all the unfinished features. It "
"might mean less stability for a while, or things shown in the user interface "
"that don't work. But in practice, there is seldom really bad instability, "
"and if it is, it's easy for you to go back to a revision that does work."
msgstr ""
"Obviamente, existem também desvantagens: quando compilar a partir do "
"repositório de código actual, irá também obter as funcionalidades "
"inacabadas. Isso poderá significar uma menor estabilidade durante algum "
"tempo ou coisas na interface que não funcionam de todo. Contudo, na prática "
"é raro haver uma instabilidade realmente má e, caso exista, será simples "
"para você voltar para uma versão que funcione."

#: ../../user_manual/getting_started/installation.rst:176
msgid ""
"So... If you want to start compiling from source, begin with the latest "
"build instructions from the guide :ref:`here <building_krita>`."
msgstr ""
"Por isso... Se quiser começar a compilar a partir do código, comece pelas "
"últimas instruções no guia :ref:`aqui <building_krita>`."

#: ../../user_manual/getting_started/installation.rst:179
msgid ""
"If you encounter any problems, or if you are new to compiling software, "
"don't hesitate to contact the Krita developers. There are three main "
"communication channels:"
msgstr ""
"Se encontrar algum problema, ou se for novo na compilação de aplicações, não "
"hesite em contactar os programadores do Krita. Existem três canais de "
"comunicação principais:"

#: ../../user_manual/getting_started/installation.rst:183
msgid "irc: irc.freenode.net, channel #krita"
msgstr "IRC: irc.freenode.net, channel #krita"

#: ../../user_manual/getting_started/installation.rst:184
msgid "`mailing list <https://mail.kde.org/mailman/listinfo/kimageshop>`__"
msgstr ""
"`lista de correio <https://mail.kde.org/mailman/listinfo/kimageshop>`__"

#: ../../user_manual/getting_started/installation.rst:185
msgid "`forums <https://forum.kde.org/viewforum.php?f=136>`__"
msgstr "`fóruns <https://forum.kde.org/viewforum.php?f=136>`__"
