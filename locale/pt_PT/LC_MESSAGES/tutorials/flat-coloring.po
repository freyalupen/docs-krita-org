# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-02 03:25+0200\n"
"PO-Revision-Date: 2019-08-03 14:29+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en image Kritamouseleft menuselection kbd\n"
"X-POFile-SpellExtra: Kritafillinglineartmask images alt\n"
"X-POFile-SpellExtra: Kritafillinglineartcolortoalpha coloring icons\n"
"X-POFile-SpellExtra: backspace beziercurve Kritafillinglineartselection\n"
"X-POFile-SpellExtra: ref Mic flat Kritafillinglineart tool Bézier path\n"
"X-POFile-SpellExtra: mouseright Krita filltool Kritamouseright\n"
"X-POFile-SpellExtra: beziercurveselectiontool mouseleft Backspace\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart14.png\n"
"   :alt: layer structure for flatting in krita"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart14.png\n"
"   :alt: estrutura de camadas para alisamento no Krita"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart1.png\n"
"   :alt: blend mode setup of line art flat coloring"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart1.png\n"
"   :alt: configuração do modo de mistura da coloração plana do desenho de "
"linhas"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart2.png\n"
"   :alt: effects of multiply blend mode"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart2.png\n"
"   :alt: efeitos do modo de mistura por multiplicação"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/icons/fill_tool.svg\n"
"   :alt: fill-tool icon"
msgstr ""
".. image:: images/icons/fill_tool.svg\n"
"   :alt: ícone da ferramenta de preenchimento"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart7.png\n"
"   :alt: colors filled with fill tool"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart7.png\n"
"   :alt: cores preenchidas com a ferramenta de preenchimento"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart15.png\n"
"   :alt: selecting with selection tools for filling color"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart15.png\n"
"   :alt: selecção com as ferramentas de selecção para o preenchimento com cor"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart16.png\n"
"   :alt: selection mask in Krita"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart16.png\n"
"   :alt: máscara de selecção no Krita"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart17.png\n"
"   :alt: filling color in selection"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart17.png\n"
"   :alt: preenchimento da cor na selecção"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart18.png\n"
"   :alt: result of coloring made with the help of selection tools"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart18.png\n"
"   :alt: resultado da coloração feita com a ajuda das ferramentas de selecção"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart8.png\n"
"   :alt: filling color in line art using path tool"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart8.png\n"
"   :alt: preenchimento com cor nos desenhos de linhas, com a ferramenta do "
"caminho"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart9.png\n"
"   :alt: erasing with path tool"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart9.png\n"
"   :alt: limpeza com a ferramenta do caminho"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart10.png\n"
"   :alt: coloring with colorize mask"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart10.png\n"
"   :alt: coloração com a máscara de coloração"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart11.png\n"
"   :alt: result from the colorize mask"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart11.png\n"
"   :alt: resultado da máscara de coloração"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart12.png\n"
"   :alt: slitting colors into islands"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart12.png\n"
"   :alt: divisão das cores em ilhas"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart13.png\n"
"   :alt: resulting color islands from split layers"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart13.png\n"
"   :alt: ilhas de cores resultantes da divisão das camadas"

#: ../../tutorials/flat-coloring.rst:0
msgid ".. image:: images/flat-coloring/Krita_filling_lineart_selection_1.png"
msgstr ".. image:: images/flat-coloring/Krita_filling_lineart_selection_1.png"

#: ../../tutorials/flat-coloring.rst:0
msgid ".. image:: images/flat-coloring/Krita_filling_lineart_selection_2.png"
msgstr ".. image:: images/flat-coloring/Krita_filling_lineart_selection_2.png"

#: ../../tutorials/flat-coloring.rst:0
msgid ".. image:: images/flat-coloring/Krita_filling_lineart_mask_1.png"
msgstr ".. image:: images/flat-coloring/Krita_filling_lineart_mask_1.png"

#: ../../tutorials/flat-coloring.rst:0
msgid ".. image:: images/flat-coloring/Krita_filling_lineart_mask_2.png"
msgstr ".. image:: images/flat-coloring/Krita_filling_lineart_mask_2.png"

#: ../../tutorials/flat-coloring.rst:0
msgid ".. image:: images/flat-coloring/Krita_filling_lineart_mask_3.png"
msgstr ".. image:: images/flat-coloring/Krita_filling_lineart_mask_3.png"

#: ../../tutorials/flat-coloring.rst:1
msgid "Common workflows used in Krita"
msgstr "Fluxos de trabalho comuns usados no Krita"

#: ../../tutorials/flat-coloring.rst:13
msgid "Flat Coloring"
msgstr "Coloração Plana"

#: ../../tutorials/flat-coloring.rst:15
msgid ""
"So you've got a cool black on white drawing, and now you want to color it! "
"The thing we’ll aim for in this tutorial is to get your line art colored in "
"with flat colors. So no shading just yet. We’ll be going through some "
"techniques for preparing the line art, and we’ll be using the layer docker "
"to put each color on a separate layer, so we can easily access each color "
"when we add shading."
msgstr ""
"Agora tem um desenho bonito em preto sobre branco, e agora deseja colori-lo! "
"O objectivo deste tutorial é ficar com o seu desenho de linhas colorido com "
"cores planas. Como tal, sem qualquer sombreado envolvido ainda. Iremos "
"percorrer algumas técnicas para preparar o desenho de linhas e iremos usar a "
"área de camadas para colocar cada uma das cores numa camada separada, para "
"que facilmente consigamos aceder a cada uma das cores quando adicionarmos o "
"sombreado."

#: ../../tutorials/flat-coloring.rst:17
msgid ""
"This tutorial is adapted from this `tutorial <http://theratutorial.tumblr."
"com/post/66584924501/flat-colouring-in-the-kingdom-of-2d-layers-are>`_ by "
"the original author."
msgstr ""
"Este tutorial é uma adaptação deste `tutorial <http://theratutorial.tumblr."
"com/post/66584924501/flat-colouring-in-the-kingdom-of-2d-layers-are>`_ do "
"autor original."

#: ../../tutorials/flat-coloring.rst:20
msgid "Understanding Layers"
msgstr "Compreender as Camadas"

#: ../../tutorials/flat-coloring.rst:22
msgid ""
"To fill line art comfortably, it's best to take advantage of the layerstack. "
"The layer stack is pretty awesome, and it's one of those features that make "
"digital art super-convenient."
msgstr ""
"Para preencher um desenho de linhas de forma confortável, é melhor tirar "
"partido da pilha de camadas. A mesma é óptima e é uma daquelas "
"funcionalidades que tornam a arte digital bastante conveniente."

#: ../../tutorials/flat-coloring.rst:24
msgid ""
"In traditional art, it is not uncommon to first draw the full background "
"before drawing the subject. Or to first draw a line art and then color it "
"in. Computers have a similar way of working."
msgstr ""
"Na arte tradicional, é normal desenhar primeiro o fundo completo antes de "
"desenhar o assunto. Ou fazer primeiro um rascunho com linhas e depois pintá-"
"lo. Os computadores têm uma forma de funcionar semelhante."

#: ../../tutorials/flat-coloring.rst:26
msgid ""
"In programming, if you tell a computer to draw a red circle, and then "
"afterwards tell it to draw a smaller yellow circle, you will see the small "
"yellow circle overlap the red circle. Switch the commands around, and you "
"will not see the yellow circle at all: it was drawn before the red circle "
"and thus ‘behind’ it."
msgstr ""
"Na programação, se diz a computador para pintar um círculo vermelho, e "
"depois lhe diz para pintar um círculo amarelo mais pequeno, irá ver o "
"pequeno círculo amarelo a sobrepor-se ao vermelho. Mude os comandos e não "
"verá o círculo amarelo de todo: foi desenhado antes do vermelho e, como tal, "
"ficou ‘atrás’ dele."

#: ../../tutorials/flat-coloring.rst:28
msgid ""
"This is referred to as the “drawing order”. So like the traditional artist, "
"the computer will first draw the images that are behind everything, and "
"layer the subject and foreground on top of it. The layer docker is a way for "
"you to control the drawing order of multiple images, so for example, you can "
"have your line art drawn later than your colors, meaning that the lines will "
"be drawn over the colors, making it easier to make it neat!"
msgstr ""
"Isto é referido como sendo a “ordem de desenho”. Por isso, como o artista "
"tradicional, o computador irá desenhar primeiro as imagens que estejam atrás "
"de tudo, colocando o assunto e os objectos principais sobre ele. A área de "
"camadas é uma forma de você controlar a ordem de desenho das várias imagens; "
"por isso, por exemplo, poderá ter o seu desenho de linhas desenhado depois "
"das suas cores, o que significa que as linhas serão desenhadas sobre as "
"cores, sendo mais simples de deixar o resultado bonito!"

#: ../../tutorials/flat-coloring.rst:30
msgid ""
"Other things that a layer stack can do are blending the colors of different "
"layers differently with blending modes, using a filter in the layer stack, "
"or using a mask that allows you to make parts transparent."
msgstr ""
"Outras coisas que uma pilha de camadas consegue fazer é misturar as cores de "
"diferentes camadas de forma diferente com os modos de mistura, usando um "
"filtro na pilha de camadas ou usando uma máscara que lhe permita tornar "
"algumas partes transparentes."

#: ../../tutorials/flat-coloring.rst:32
msgid ""
"Programmers talk about transparency as ''Alpha'', which is because the 'a' "
"symbol is used to present transparency in the algorithms for painting one "
"color on top of another. Usually when you see the word ''Alpha'' in a "
"graphics program, just think of it as affecting the transparency."
msgstr ""
"Os programadores referem-se à transparência como ''Alfa'', porque o símbolo "
"'a' é usado para representar a transparência nos algoritmos para pintar uma "
"cor sobre outra. Normalmente, quando vê a palavra ''Alfa'' num programa "
"gráfico, pense nela como afectando a transparência."

#: ../../tutorials/flat-coloring.rst:35
msgid "Preparing your line art"
msgstr "Preparar o seu desenho de linhas"

#: ../../tutorials/flat-coloring.rst:37
msgid ""
"Put the new layer underneath the layer containing the line art (drag and "
"drop or use the up/down arrows for that), and draw on it."
msgstr ""
"Coloque a nova camada por baixo da camada que contém o desenho de linhas "
"(arraste e largue ou use as setas para cima/baixo para esse fim) e desenhe "
"sobre ele."

#: ../../tutorials/flat-coloring.rst:42
msgid ""
"…And notice nothing happening. This is because the white isn’t transparent. "
"You wouldn’t really want it to either, how else would you make convincing "
"highlights? So what we first need to do to color in our drawing is prepare "
"our line art. There’s several methods of doing so, each with varying "
"qualities."
msgstr ""
"…E repare que nada aconteceu. Isto acontece porque o branco não é "
"transparente. Também não desejaria que o fosse, senão como iria obter tons "
"claros convincentes? Por isso, primeiro precisamos de fazer para colorir o "
"nosso desenho é preparar o nosso desenho de linhas. Existem vários métodos "
"para o fazer, cada um com qualidades variadas."

#: ../../tutorials/flat-coloring.rst:45
msgid "The Multiply Blending Mode"
msgstr "O Modo de Mistura por Multiplicação"

#: ../../tutorials/flat-coloring.rst:47
msgid ""
"So, typically, to get a black and white line art usable for coloring, you "
"can set the blending mode of the line art layer to Multiply. You do this by "
"selecting the layer and going to the drop-down that says **Normal** and "
"setting that to **Multiply**."
msgstr ""
"Assim, tipicamente para obter um desenho de linhas a preto-e-branco apto "
"para ser colorido, poderá definir o modo de mistura da camada de desenho "
"como Multiplicação. Poderá fazer isto ao seleccionar a camada e depois ir à "
"lista que diz **Normal** e mudar o valor para **Multiplicação**."

#: ../../tutorials/flat-coloring.rst:52
msgid "And then you should be able to see your colors!"
msgstr "E assim sendo deverá conseguir ver as suas cores!"

#: ../../tutorials/flat-coloring.rst:54
msgid ""
"Multiply is not a perfect solution however. For example, if through some "
"image editing magic I make the line art blue, it results into this:"
msgstr ""
"A multiplicação não é contudo uma solução perfeita. Por exemplo, se por "
"alguma magia na edição de imagens eu tiver tornado o desenho de linhas azul, "
"irá resultar nisto:"

#: ../../tutorials/flat-coloring.rst:59
msgid ""
"This is because multiply literally multiplies the colors. So it uses maths!"
msgstr ""
"Isto é porque a multiplicação literalmente multiplica as cores. Como tal, "
"faz contas matemáticas!"

#: ../../tutorials/flat-coloring.rst:61
msgid ""
"What it first does is take the values of the RGB channels, then divides them "
"by the max (because we're in 8bit, this is 255), a process we call "
"normalising. Then it multiplies the normalized values. Finally, it takes the "
"result and multiplies it with 255 again to get the result values."
msgstr ""
"O que ele faz primeiro é pegar nos valores dos canais RGB, dividi-los pelo "
"valor máximo (como estamos em 8 bits, corresponde a 255), chamando-se este "
"processo de normalização. Depois, multiplica os valores normalizados. "
"Finalmente, pega no resultado e multiplica de novo por 255 para obter os "
"valores do resultado."

#: ../../tutorials/flat-coloring.rst:67
msgid "Pink"
msgstr "Rosa"

#: ../../tutorials/flat-coloring.rst:68
msgid "Pink (normalized)"
msgstr "Rosa (normalizado)"

#: ../../tutorials/flat-coloring.rst:69 ../../tutorials/flat-coloring.rst:87
msgid "Blue"
msgstr "Azul"

#: ../../tutorials/flat-coloring.rst:70
msgid "Blue (normalized)"
msgstr "Azul (normalizado)"

#: ../../tutorials/flat-coloring.rst:71
msgid "Normalized, multiplied"
msgstr "Normalizado, multiplicado"

#: ../../tutorials/flat-coloring.rst:72
msgid "Result"
msgstr "Resultado"

#: ../../tutorials/flat-coloring.rst:73
msgid "Red"
msgstr "Vermelho"

#: ../../tutorials/flat-coloring.rst:74
msgid "222"
msgstr "222"

#: ../../tutorials/flat-coloring.rst:75
msgid "0.8705"
msgstr "0,8705"

#: ../../tutorials/flat-coloring.rst:76
msgid "92"
msgstr "92"

#: ../../tutorials/flat-coloring.rst:77
msgid "0.3607"
msgstr "0,3607"

#: ../../tutorials/flat-coloring.rst:78
msgid "0.3139"
msgstr "0,3139"

#: ../../tutorials/flat-coloring.rst:79
msgid "80"
msgstr "80"

#: ../../tutorials/flat-coloring.rst:80
msgid "Green"
msgstr "Verde"

#: ../../tutorials/flat-coloring.rst:81
msgid "144"
msgstr "144"

#: ../../tutorials/flat-coloring.rst:82
msgid "0.5647"
msgstr "0,5647"

#: ../../tutorials/flat-coloring.rst:83
msgid "176"
msgstr "176"

#: ../../tutorials/flat-coloring.rst:84
msgid "0.6902"
msgstr "0,6902"

#: ../../tutorials/flat-coloring.rst:85
msgid "0.3897"
msgstr "0,3897"

#: ../../tutorials/flat-coloring.rst:86
msgid "99"
msgstr "99"

#: ../../tutorials/flat-coloring.rst:88
msgid "123"
msgstr "123"

#: ../../tutorials/flat-coloring.rst:89
msgid "0.4823"
msgstr "0,4823"

#: ../../tutorials/flat-coloring.rst:90
msgid "215"
msgstr "215"

#: ../../tutorials/flat-coloring.rst:91
msgid "0.8431"
msgstr "0,8431"

#: ../../tutorials/flat-coloring.rst:92
msgid "0.4066"
msgstr "0,4066"

#: ../../tutorials/flat-coloring.rst:93
msgid "103"
msgstr "103"

#: ../../tutorials/flat-coloring.rst:95
msgid ""
"This isn't completely undesirable, and a lot of artists use this effect to "
"add a little richness to their colors."
msgstr ""
"Isto não é totalmente indesejável, e muitos artistas usam este efeito para "
"atribuir alguma riqueza às suas cores."

#: ../../tutorials/flat-coloring.rst:98 ../../tutorials/flat-coloring.rst:127
#: ../../tutorials/flat-coloring.rst:154 ../../tutorials/flat-coloring.rst:173
#: ../../tutorials/flat-coloring.rst:210 ../../tutorials/flat-coloring.rst:247
#: ../../tutorials/flat-coloring.rst:275 ../../tutorials/flat-coloring.rst:317
msgid "Advantages"
msgstr "Vantagens"

#: ../../tutorials/flat-coloring.rst:100
msgid ""
"Easy, can work to your benefit even with colored lines by softening the look "
"of the lines while keeping nice contrast."
msgstr ""
"Simples e pode funcionar em seu benefício, mesmo com linhas coloridas, "
"suavizando a aparência das linhas, embora mantenha um bom contraste."

#: ../../tutorials/flat-coloring.rst:103 ../../tutorials/flat-coloring.rst:132
#: ../../tutorials/flat-coloring.rst:159 ../../tutorials/flat-coloring.rst:178
#: ../../tutorials/flat-coloring.rst:215 ../../tutorials/flat-coloring.rst:252
#: ../../tutorials/flat-coloring.rst:280 ../../tutorials/flat-coloring.rst:322
msgid "Disadvantages"
msgstr "Desvantagens"

#: ../../tutorials/flat-coloring.rst:105
msgid "Not actually transparent. Is a little funny with colored lines."
msgstr ""
"Não é de facto transparente. Fica um pouco engraçado com as linhas coloridas."

#: ../../tutorials/flat-coloring.rst:108
msgid "Using Selections"
msgstr "Usar as Selecções"

#: ../../tutorials/flat-coloring.rst:110
msgid ""
"The second method is one where we'll make it actually transparent. In other "
"programs this would be done via the channel docker, but Krita doesn't do "
"custom channels, instead it uses Selection Masks to store custom selections."
msgstr ""
"O segundo método é um em que o iremos tornar realmente transparente. Noutros "
"programas, isto seria feito com a área de canais, mas o Krita não possui "
"canais personalizados; em vez disso, usa Máscaras de Selecção para guardar "
"as selecções personalizadas."

#: ../../tutorials/flat-coloring.rst:112
msgid "Duplicate your line art layer."
msgstr "Duplique a sua camada do desenho de linhas."

#: ../../tutorials/flat-coloring.rst:114
msgid ""
"Convert the duplicate to a selection mask. |mouseright| the layer, then :"
"menuselection:`Convert --> to Selection Mask`."
msgstr ""
"Converta o duplicado para uma máscara de selecção. Use o |mouseright| sobre "
"a camada, e depois :menuselection:`Converter --> para Máscara de Selecção`."

#: ../../tutorials/flat-coloring.rst:118
msgid ""
"Invert the selection mask. :menuselection:`Select --> Invert Selection`."
msgstr ""
"Inverta a máscara de selecção. :menuselection:`Seleccionar --> Inverter a "
"Selecção`."

#: ../../tutorials/flat-coloring.rst:120
msgid ""
"Make a new layer, and do :menuselection:`Edit --> Fill with Foreground "
"Color`."
msgstr ""
"Crie uma nova camada e invoque o :menuselection:`Editar --> Preencher com a "
"Cor Principal`."

#: ../../tutorials/flat-coloring.rst:124
msgid "And you should now have the line art on a separate layer."
msgstr "E agora deverá ter o seu desenho de linhas numa camada separada."

#: ../../tutorials/flat-coloring.rst:129
msgid "Actual transparency."
msgstr "Transparência actual."

#: ../../tutorials/flat-coloring.rst:134
msgid "Doesn't work when the line art is colored."
msgstr "Não resulta quando o desenho de linhas é colorido."

#: ../../tutorials/flat-coloring.rst:137
msgid "Using Masks"
msgstr "Usar as Máscaras"

#: ../../tutorials/flat-coloring.rst:139
msgid "This is a simpler variation of the above."
msgstr "Esta é uma versão mais simplificada da anterior."

#: ../../tutorials/flat-coloring.rst:141
msgid "Make a filled layer underneath the line art layer."
msgstr "Crie uma camada preenchida por baixo da camada do desenho de linhas."

#: ../../tutorials/flat-coloring.rst:145
msgid ""
"Convert the line art layer to a transparency mask |mouseright| the layer, "
"then :menuselection:`Convert --> to Transparency Mask`."
msgstr ""
"Converta a camada do desenho de linhas para uma máscara de transparência com "
"o |mouseright| sobre a camada e depois invocando :menuselection:`Converter --"
"> para Máscara de Transparência`."

#: ../../tutorials/flat-coloring.rst:149
msgid ""
"Invert the transparency mask by going to :menuselection:`Filter --> Adjust --"
"> Invert`"
msgstr ""
"Inverta a máscara de transparência com a opção :menuselection:`Filtro --> "
"Ajustar --> Inverter`"

#: ../../tutorials/flat-coloring.rst:156
msgid ""
"Actual transparency. You can also very easily doodle a pattern on the filled "
"layer where the mask is on without affecting the transparency."
msgstr ""
"Transparência actual. Também poderá aplicar facilmente um padrão sobre a "
"camada preenchida, onde a máscara esteja activa sem afectar a transparência."

#: ../../tutorials/flat-coloring.rst:161
msgid ""
"Doesn't work when the line art is colored already. We can still get faster."
msgstr ""
"Não funciona quando o desenho de linhas já é colorido. Também podemos "
"conseguir acelerar o processo."

#: ../../tutorials/flat-coloring.rst:164
msgid "Using Color to Alpha"
msgstr "Usar a Cor para o Alfa"

#: ../../tutorials/flat-coloring.rst:166
msgid "By far the fastest way to get transparent line art."
msgstr ""
"De longe a forma mais rápida de tornar um desenho de linhas transparente."

#: ../../tutorials/flat-coloring.rst:168
msgid ""
"Select the line art layer and apply the color to alpha filter. :"
"menuselection:`Filter --> Colors --> Color to Alpha`. The default values "
"should be sufficient for line art."
msgstr ""
"Seleccione a camada do desenho de linhas e aplique o filtro de cor para "
"alfa. :menuselection:`Filtro --> Cores --> Cor para o Alfa`. Os valores "
"predefinidos deverão ser suficientes para o desenho de linhas."

#: ../../tutorials/flat-coloring.rst:171
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart_color_to_alpha.png"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart_color_to_alpha.png"

#: ../../tutorials/flat-coloring.rst:175
msgid ""
"Actual transparency. Works with colored line art as well, because it removes "
"the white specifically."
msgstr ""
"Transparência actual. Funciona também com desenhos coloridos, porque remove "
"o branco em particular."

#: ../../tutorials/flat-coloring.rst:180
msgid ""
"You'll have to lock the layer transparency or separate out the alpha via the "
"right-click menu if you want to easily color it."
msgstr ""
"Terá de bloquear a transparência da camada ou separar o alfa através do menu "
"do botão direito, caso queira colori-lo facilmente."

#: ../../tutorials/flat-coloring.rst:184
msgid "Coloring the image"
msgstr "Colorir a imagem"

#: ../../tutorials/flat-coloring.rst:186
msgid ""
"Much like preparing the line art, there are many different ways of coloring "
"a layer."
msgstr ""
"Tal como na preparação do desenho de linhas, existem muitas formas de "
"colorir uma camada."

#: ../../tutorials/flat-coloring.rst:188
msgid ""
"You could for example fill in everything by hand, but while that is very "
"precise it also takes a lot of work. Let's take a look at the other options, "
"shall we?"
msgstr ""
"Poderá por exemplo preencher tudo à mão, mas embora isso seja muito preciso, "
"também dá muito trabalho. Vejamos as outras opções, ok?"

#: ../../tutorials/flat-coloring.rst:191
msgid "Fill Tool"
msgstr "Ferramenta de Preenchimento"

#: ../../tutorials/flat-coloring.rst:196
msgid ""
"In most cases the fill-tool can’t deal with the anti-aliasing (the soft edge "
"in your line art to make it more smooth when zoomed out) In Krita you have "
"the grow-shrink option. Setting that to say… 2 expands the color two pixels."
msgstr ""
"Na maioria dos casos a ferramenta de preenchimento não consegue lidar com a "
"suavização (as arestas no seu desenho de linhas tendem a ficar mais suaves "
"quando reduz a ampliação). No Krita, tem a opção para 'crescimento/"
"diminuição'. Se configurar esse valor para p.ex. … 2 expande a cor em dois "
"pixels."

#: ../../tutorials/flat-coloring.rst:198
msgid ""
"Threshold decides when the fill-tool should consider a different color pixel "
"to be a border. And the feathering adds an extra soft border to the fill."
msgstr ""
"O limite decide quando a ferramenta de preenchimento deverá considerar uma "
"cor diferente como sendo um contorno. E a suavização ainda atribuir uma "
"camada extra suave ao preenchimento."

#: ../../tutorials/flat-coloring.rst:200
msgid ""
"Now, if you click on a gapless-part of the image with your preferred color… "
"(Remember to set the opacity to 1.0!)"
msgstr ""
"Agora, se carregar numa parte sem lacunas da imagem com a sua cor preferida… "
"(Lembre-se de configurar a opacidade como 1,0!)"

#: ../../tutorials/flat-coloring.rst:202
msgid ""
"Depending on your line art, you can do flats pretty quickly. But setting the "
"threshold low can result in little artifacts around where lines meet:"
msgstr ""
"Dependendo do seu desenho de linhas, poderá fazer pinturas planas de forma "
"bastante rápida. Mas a definição do limite como baixo poderá resultar em "
"pequenos artefactos aqui e ali onde as linhas se encontram:"

#: ../../tutorials/flat-coloring.rst:207
msgid ""
"However, setting the threshold high can end with the fill not recognizing "
"some of the lighter lines. Besides these little artifacts can be removed "
"with the brush easily."
msgstr ""
"Contudo, a definição do limite como alto poderá fazer com que o "
"preenchimento não reconheça algumas das linhas mais claras. Para além disso, "
"estes pequenos artefactos poderão ser removidos facilmente com o pincel."

#: ../../tutorials/flat-coloring.rst:212
msgid "Pretty darn quick depending on the available settings."
msgstr "Bastante rápido, dependendo da configuração disponível."

#: ../../tutorials/flat-coloring.rst:217
msgid ""
"Again, not great with gaps or details. And it works best with aliased line "
"art."
msgstr ""
"Mais uma vez, não é óptimo com as lacunas ou detalhes. E funciona melhor com "
"desenhos de linhas não-suavizados."

#: ../../tutorials/flat-coloring.rst:220
msgid "Selections"
msgstr "Selecções"

#: ../../tutorials/flat-coloring.rst:222
msgid "Selections work using the selection tools."
msgstr "As selecções funcionam com as ferramentas de selecção."

#: ../../tutorials/flat-coloring.rst:227
msgid ""
"For example with the :ref:`bezier_curve_selection_tool` you can easily "
"select a curved area, and the with :kbd:`Shift +` |mouseleft| (not |"
"mouseleft| :kbd:`+ Shift`, there's a difference!) you can easily add to an "
"existing selection."
msgstr ""
"Por exemplo, com a :ref:`bezier_curve_selection_tool`, poderá seleccionar "
"facilmente uma área curvada, e com o :kbd:`Shift` + |mouseleft| (não é |"
"mouseleft| + :kbd:`Shift`, há uma diferença!), poderá facilmente adicionar "
"uma selecção existente."

#: ../../tutorials/flat-coloring.rst:232
msgid ""
"You can also edit the selection if you have :menuselection:`Select --> Show "
"Global Selection Mask` turned on. Then you can select the global selection "
"mask, and paint on it. (Above with the alternative selection mode, activated "
"in the lower-left corner of the stats bar)"
msgstr ""
"Poderá também editar a selecção se tiver a opção :menuselection:`Seleccionar "
"--> Mostrar a Máscara de Selecção Global` activada. Depois poderá "
"seleccionar a máscara de selecção global e pintar sobre ela. (Como acima, "
"com o modo de selecção alternativo, activado no canto inferior-esquerdo da "
"barra de estado)"

#: ../../tutorials/flat-coloring.rst:234
msgid ""
"When done, select the color you want to fill it with and press the :kbd:"
"`Shift + Backspace` shortcut."
msgstr ""
"Quando terminar, seleccione a cor com que deseja preencher e carregue em :"
"kbd:`Shift + Backspace`."

#: ../../tutorials/flat-coloring.rst:239
msgid ""
"You can save selections in selection masks by |mouseright| a layer, and then "
"going to :menuselection:`Add --> Local Selection`. You first need to "
"deactivate a selection by pressing the circle before adding a new selection."
msgstr ""
"Poderá gravar as selecções como máscaras de selecção com o |mouseright| "
"sobre uma camada e depois ir a :menuselection:`Adicionar --> Selecção "
"Local`. Primeiro precisa de desactivar uma selecção, carregando no círculo "
"antes de adicionar uma nova selecção."

#: ../../tutorials/flat-coloring.rst:241
msgid ""
"This can serve as an alternative way to split out different parts of the "
"image, which is good for more painterly pieces:"
msgstr ""
"Isto pode servir como uma alternativa para dividir as diferentes partes da "
"imagem, o que é bom para peças com mais pinturas:"

#: ../../tutorials/flat-coloring.rst:249
msgid "A bit more precise than filling."
msgstr "Um pouco mais preciso que o preenchimento."

#: ../../tutorials/flat-coloring.rst:254
msgid "Previewing your color isn't as easy."
msgstr "A antevisão da sua cor não é simples."

#: ../../tutorials/flat-coloring.rst:257
msgid "Geometric tools"
msgstr "Ferramentas geométricas"

#: ../../tutorials/flat-coloring.rst:259
msgid ""
"So you have a tool for making rectangles or circles. And in the case of "
"Krita, a tool for bezier curves. Select the path tool (|path tool|), and set "
"the tool options to fill=foreground and outline=none. Make sure that your "
"opacity is set to 1.00 (fully opaque)."
msgstr ""
"Bem, tem uma ferramenta para criar rectângulos ou círculos. E, no caso do "
"Krita, uma ferramenta para curvas Bézier. Seleccione a ferramenta do caminho "
"(|path tool|), e configure as opções da ferramenta para "
"preenchimento=principal e contorno=nenhum. Certifique-se que a sua opacidade "
"está configurada como 1,00 (completamente opaca)."

#: ../../tutorials/flat-coloring.rst:262
msgid ""
".. image:: images/icons/bezier_curve.svg\n"
"   :alt: path tool"
msgstr ""
".. image:: images/icons/bezier_curve.svg\n"
"   :alt: ferramenta do caminho"

#: ../../tutorials/flat-coloring.rst:264
msgid ""
"By clicking and holding, you can influence how curvy a line draw with the "
"path tool is going to be. Letting go of the mouse button confirms the "
"action, and then you’re free to draw the next point."
msgstr ""
"Ao carregar e manter carregado, poderá influenciar quão curva ficará uma "
"linha desenhada pela ferramenta do caminho. Se largar o botão do rato, irá "
"confirmar a acção, ficando livre para desenhar o ponto seguinte."

#: ../../tutorials/flat-coloring.rst:269
msgid ""
"You can also erase with a geometric tool. Just press the :kbd:`E` key or the "
"eraser button."
msgstr ""
"Também poderá apagar com uma ferramenta geométrica. Basta carregar em :kbd:"
"`E` ou no botão da borracha."

#: ../../tutorials/flat-coloring.rst:277
msgid ""
"Quicker than using the brush or selections. Also decent with line art that "
"contains gaps."
msgstr ""
"Mais rápido do que usar o pincel ou as selecções. Também é decente com "
"desenhos de linhas que contenham lacunas."

#: ../../tutorials/flat-coloring.rst:282
msgid ""
"Fiddly details aren’t easy to fill in with this. So I recommend skipping "
"those and filling them in later with a brush."
msgstr ""
"Alguns detalhes serão mais complicados de preencher com isto. Como tal, "
"recomenda-se que os deixe e os preencha mais tarde com um pincel."

#: ../../tutorials/flat-coloring.rst:285
msgid "Colorize Mask"
msgstr "Máscara de Coloração"

#: ../../tutorials/flat-coloring.rst:287
msgid ""
"So, this is a bit of an odd one. In the original tutorial, you'll see I'm "
"suggesting using G'Mic, but that was a few years ago, and G'Mic is a little "
"unstable on windows. Therefore, the Krita developers have been attempting to "
"make an internal tool doing the same."
msgstr ""
"Bem, esta é um pouco estranha. No tutorial original, verá que estou a "
"sugerir o uso do G'Mic, mas isso já foi há alguns anos, e o G'Mic tornou-se "
"um pouco instável no Windows. Como tal, os programadores do Krita têm estado "
"a tentar criar uma ferramenta interna que faça o mesmo."

#: ../../tutorials/flat-coloring.rst:289
msgid ""
"It is disabled in 3.1, but if you use 4.0 or later, it is in the toolbox. "
"Check the Colorize Mask for more information."
msgstr ""
"Está desactivada no 3.1, mas se usar a 4.0 ou posterior, encontra-se na área "
"de ferramentas. Veja mais informações na Máscara de Coloração."

#: ../../tutorials/flat-coloring.rst:291
msgid "So it works like this:"
msgstr "Sendo assim, funciona da seguinte forma:"

#: ../../tutorials/flat-coloring.rst:293
msgid "Select the colorize mask tool."
msgstr "Seleccione a máscara de coloração."

#: ../../tutorials/flat-coloring.rst:294
msgid "Tick the layer you're using."
msgstr "Carregue na camada que está a usar."

#: ../../tutorials/flat-coloring.rst:295
msgid "Paint the colors you want to use on the colorize mask"
msgstr "Pinte as cores que quer usar na máscara de coloração"

#: ../../tutorials/flat-coloring.rst:296
msgid "Click update to see the results:"
msgstr "Carregue em Actualizar para ver os resultados:"

#: ../../tutorials/flat-coloring.rst:301
msgid ""
"When you are satisfied, |mouseright| the colorize mask, and go to :"
"menuselection:`Convert --> Paint Layer`. This will turn the colorize mask to "
"a generic paint layer. Then, you can fix the last issues by making the line "
"art semi-transparent and painting the flaws away with a pixel art brush."
msgstr ""
"Quando estiver satisfeito, carregue com o |mouseright| sobre a máscara de "
"coloração e vá a :menuselection:`Converter --> Camada de Pintura`. Isto irá "
"transformar a máscara de coloração numa máscara de pintura genérica. Depois, "
"poderá corrigir os últimos problemas, tornando o desenho de linhas semi-"
"transparente e pintando os defeitos com um pincel de pixels."

#: ../../tutorials/flat-coloring.rst:306
msgid ""
"Then, when you are done, split the layers via :menuselection:`Layer --> "
"Split --> Split Layer`. There are a few options you can choose, but the "
"following should be fine:"
msgstr ""
"Depois, quando terminar, divida as camadas com a opção :menuselection:"
"`Camadas --> Dividir --> Dividir a Camada`. Existem algumas opções que "
"poderá escolher, mas as seguintes deverão ser suficientes:"

#: ../../tutorials/flat-coloring.rst:311
msgid ""
"Finally, press **Ok** and you should get the following. Each color patch it "
"on a different layer, named by the palette in the menu and alpha locked, so "
"you can start painting right away!"
msgstr ""
"Finalmente, carregue em **Ok** para obter o seguinte. Cada padrão de cor "
"ficou numa camada diferente, com o nome da paleta no menu e bloqueada no "
"alfa, para que possa começar a pintar desde já!"

#: ../../tutorials/flat-coloring.rst:319
msgid ""
"Works with anti-aliased line art. Really quick to get the base work done. "
"Can auto-close gaps."
msgstr ""
"Funciona com desenhos de linhas suavizados. É bastante rápido para ter o "
"trabalho de base feito. Pode fechar automaticamente as lacunas."

#: ../../tutorials/flat-coloring.rst:324
msgid ""
"No anti-aliasing of its own. You have to choose between getting details "
"right or the gaps auto-closed."
msgstr ""
"Sem suavização própria. Terá de optar entre ficar com os detalhes corrigidos "
"ou as lacunas fechadas automaticamente."

#: ../../tutorials/flat-coloring.rst:327
msgid "Conclusion"
msgstr "Conclusão"

#: ../../tutorials/flat-coloring.rst:329
msgid ""
"I hope this has given you a good idea of how to fill in flats using the "
"various techniques, as well as getting a hand of different Krita features. "
"Remember that a good flat filled line art is better than a badly shaded one, "
"so keep practicing to get the best out of these techniques!"
msgstr ""
"Espero que isto lhe tenha dado uma boa ideia de como preencher partes planas "
"com várias técnicas, assim como passar a conhecer um conjunto de "
"funcionalidades diferentes do Krita. Lembre-se que um bom desenho de linhas "
"bem preenchido é melhor que um mal-feito, por isso continue a praticar para "
"desfrutar ao máximo destas técnicas!"
