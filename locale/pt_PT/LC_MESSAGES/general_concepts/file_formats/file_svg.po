# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 11:40+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: ref Krita SVGs menuselection Scalable Graphics\n"
"X-POFile-SpellExtra: Inkscape\n"

#: ../../general_concepts/file_formats/file_svg.rst:1
msgid "The Scalable Vector Graphics file format in Krita."
msgstr "O formato de ficheiros de Gráficos Vectoriais Escaláveis no Krita."

#: ../../general_concepts/file_formats/file_svg.rst:10
msgid "SVG"
msgstr "SVG"

#: ../../general_concepts/file_formats/file_svg.rst:10
msgid "*.svg"
msgstr "*.svg"

#: ../../general_concepts/file_formats/file_svg.rst:10
msgid "Scalable Vector Graphics Format"
msgstr "Formato de Ficheiros de Gráficos Vectoriais Escaláveis"

#: ../../general_concepts/file_formats/file_svg.rst:15
msgid "\\*.svg"
msgstr "\\*.svg"

#: ../../general_concepts/file_formats/file_svg.rst:17
msgid ""
"``.svg``, or Scalable Vector Graphics, is the most modern vector graphics "
"interchange file format out there."
msgstr ""
"O ``.svg``, ou Scalable Vector Graphics (Gráficos Vectoriais Escaláveis), é "
"o formato de intercâmbio de gráficos vectoriais mais moderno que existe por "
"aí."

#: ../../general_concepts/file_formats/file_svg.rst:19
msgid ""
"Being vector graphics, SVG is very light weight. This is because it usually "
"only stores coordinates and parameters for the maths involved with vector "
"graphics."
msgstr ""
"Sendo gráficos vectoriais, o formato SVG é bastante leve. Isto acontece "
"porque normalmente só guarda coordenadas e parâmetros para as contas "
"matemáticas envolvidas nos gráficos vectoriais."

#: ../../general_concepts/file_formats/file_svg.rst:21
msgid ""
"It is maintained by the W3C SVG working group, who also maintain other open "
"standards that make up our modern internet."
msgstr ""
"O formato é mantido pelo grupo de trabalho do SVG pela W3C, que também "
"mantém algumas das outras normas abertas que compõem a nossa Internet "
"moderna."

#: ../../general_concepts/file_formats/file_svg.rst:23
msgid ""
"While you can open up SVG files with any text-editor to edit them, it is "
"best to use a vector program like Inkscape. Krita 2.9 to 3.3 supports "
"importing SVG via the add shape docker. Since Krita 4.0, SVGs can be "
"properly imported, and you can export singlevector layers via :menuselection:"
"`Layer --> Import/Export --> Save Vector Layer as SVG...`. For 4.0, Krita "
"will also use SVG to save vector data into its :ref:`internal format "
"<file_kra>`."
msgstr ""
"Embora possa abrir os ficheiros SVG com qualquer editor de texto para os "
"editar, é melhor usar um programa vectorial como o Inkscape. O Krita 2.9 até "
"ao 3.3 suporta a importação de SVG através da área de adição de formas. "
"Desde o Krita 4.0, os SVGs podem ser devidamente importados e poderá "
"exportar camadas vectoriais individuais com a opção :menuselection:`Camada --"
"> Importar/Exportar --> Gravar a Camada Vectorial como SVG...`. Para o 4.0, "
"o Krita também irá usar o SVG para gravar os dados vectoriais no seu "
"próprio :ref:`formato interno <file_kra>`."

#: ../../general_concepts/file_formats/file_svg.rst:25
msgid ""
"SVG is designed for the internet, though sadly, because vector graphics are "
"considered a bit obscure compared to raster graphics, not a lot of websites "
"accept them yet. Hosting them on your own webhost works just fine though."
msgstr ""
"O SVG está desenhado para a Internet, ainda que infelizmente seja "
"considerado um pouco obscuro em comparação com os gráficos rasterizados; não "
"são muitas as páginas que já os aceitam. O alojamento dos mesmos no seu "
"servidor Web funciona bem, de qualquer forma."
