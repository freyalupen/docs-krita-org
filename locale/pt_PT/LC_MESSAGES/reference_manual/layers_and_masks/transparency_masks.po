# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-02 23:41+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Krita menuselection\n"

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:1
msgid "How to use transparency masks in Krita."
msgstr "Como usar as máscaras de transparência no Krita."

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:15
msgid "Layers"
msgstr "Camadas"

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:15
msgid "Masks"
msgstr "Máscaras"

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:15
msgid "Transparency"
msgstr "Transparência"

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:20
msgid "Transparency Masks"
msgstr "Máscaras de Transparência"

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:22
msgid ""
"The Transparency mask allows you to selectively show or hide parts of a "
"layer.  By using a mask, you are able to avoid deleting parts of an image "
"that you just might want in the future. This allows you to work non-"
"destructively."
msgstr ""
"A máscara de Transparência permite-lhe mostrar ou esconder, de forma "
"selectiva, partes de uma camada. Ao usar uma máscara, poderá evitar apagar "
"partes de uma imagem que possa desejar no futuro. Isto permite-lhe trabalhar "
"de forma não-destrutiva."

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:24
msgid ""
"In addition, it allows you to do things like remove a portion of a layer in "
"the layer stack so you can see what's behind it. One example would be if you "
"wanted to replace a sky, but were unsure of how much you wanted to replace."
msgstr ""
"Para além disso, permite-lhe fazer algumas coisas como a remoção de parte de "
"uma camada na pilha de camadas, para que possa ver o que se passa atrás "
"dela. Um exemplo seria se quisesse substituir um céu, mas não tivesse a "
"certeza de quanto é que desejaria substituir do mesmo."

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:28
msgid "How to add a transparency mask"
msgstr "Como adicionar uma máscara de transparência"

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:30
msgid "Click on a paint layer in the layers docker."
msgstr "Carregue numa camada de pintura na área de camadas."

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:31
msgid ""
"Click on \"+\" drop-down in the bottom left corner of the layers docker and "
"choose :menuselection:`Transparency Mask`."
msgstr ""
"Carregue na lista \"+\" do canto inferior esquerdo da área de camadas e "
"escolha :menuselection:`Máscara de Transparência`."

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:32
msgid ""
"Use your preferred paint tool to paint on the canvas. Black paints "
"transparency (see-through), white paints opacity (visible). Gray values "
"paint semi-transparency."
msgstr ""
"Use a sua ferramenta de pintura preferida para pintar na área de desenho. O "
"preto pinta a transparência (para ver através) e o branco pinta a opacidade "
"(visível). Os tons de cinzento pintam a semi-transparência."

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:35
msgid ""
"You can always fine-tune and edit what you want visible and any layer. If "
"you discover you've hidden part of your paint layer accidentally, you can "
"always show it again just by painting white on your transparency mask."
msgstr ""
"Poderá sempre afinar e editar o que deseja ter visível em qualquer camada. "
"Se descobrir que escondeu parte da sua camada de pintura sem querer, poderá "
"sempre mostrá-la de novo, bastando para tal pintar de branco na sua máscara "
"de transparência."

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:37
msgid ""
"This makes for a workflow that is extremely flexible and tolerant of "
"mistakes."
msgstr "Isto dá origem a um fluxo bastante flexível e tolerante a erros."
