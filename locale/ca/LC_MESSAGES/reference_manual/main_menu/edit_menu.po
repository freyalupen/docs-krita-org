# Translation of docs_krita_org_reference_manual___main_menu___edit_menu.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-14 03:19+0200\n"
"PO-Revision-Date: 2019-08-14 15:10+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../reference_manual/main_menu/edit_menu.rst:1
msgid "The edit menu in Krita."
msgstr "El menú Edita en el Krita."

#: ../../reference_manual/main_menu/edit_menu.rst:11
#: ../../reference_manual/main_menu/edit_menu.rst:19
msgid "Undo"
msgstr "Desfés"

#: ../../reference_manual/main_menu/edit_menu.rst:11
#: ../../reference_manual/main_menu/edit_menu.rst:22
msgid "Redo"
msgstr "Refés"

#: ../../reference_manual/main_menu/edit_menu.rst:11
#: ../../reference_manual/main_menu/edit_menu.rst:25
msgid "Cut"
msgstr "Retalla"

#: ../../reference_manual/main_menu/edit_menu.rst:11
#: ../../reference_manual/main_menu/edit_menu.rst:28
msgid "Copy"
msgstr "Copia"

#: ../../reference_manual/main_menu/edit_menu.rst:11
#: ../../reference_manual/main_menu/edit_menu.rst:40
msgid "Paste"
msgstr "Enganxa"

#: ../../reference_manual/main_menu/edit_menu.rst:11
msgid "Edit"
msgstr "Edita"

#: ../../reference_manual/main_menu/edit_menu.rst:16
msgid "Edit Menu"
msgstr "El menú Edita"

#: ../../reference_manual/main_menu/edit_menu.rst:21
msgid "Undoes the last action. Shortcut: :kbd:`Ctrl + Z`"
msgstr "Desfà l'última acció. Drecera: :kbd:`Ctrl + Z`"

#: ../../reference_manual/main_menu/edit_menu.rst:24
msgid "Redoes the last undone action. Shortcut: :kbd:`Ctrl + Shift+ Z`"
msgstr "Refà l'última acció. Drecera: :kbd:`Ctrl + Majús. + Z`"

#: ../../reference_manual/main_menu/edit_menu.rst:27
msgid "Cuts the selection or layer. Shortcut: :kbd:`Ctrl + X`"
msgstr "Retalla la selecció o capa. Drecera: :kbd:`Ctrl + X`"

#: ../../reference_manual/main_menu/edit_menu.rst:30
msgid "Copies the selection or layer. Shortcut: :kbd:`Ctrl + C`"
msgstr "Copia la selecció o capa. Drecera: :kbd:`Ctrl + C`"

#: ../../reference_manual/main_menu/edit_menu.rst:31
msgid "Cut (Sharp)"
msgstr "Retalla (afilat)"

#: ../../reference_manual/main_menu/edit_menu.rst:33
msgid ""
"This prevents semi-transparent areas from appearing on your cut pixels, "
"making them either fully opaque or fully transparent."
msgstr ""
"Evita que apareguin àrees semitransparents en els píxels retallats, el que "
"els fa totalment opacs o totalment transparents."

#: ../../reference_manual/main_menu/edit_menu.rst:34
msgid "Copy (Sharp)"
msgstr "Copia (afilada)"

#: ../../reference_manual/main_menu/edit_menu.rst:36
msgid "Same as :term:`Cut (Sharp)` but then copying instead."
msgstr "El mateix que :term:`Retalla (afilat)` però després copia."

#: ../../reference_manual/main_menu/edit_menu.rst:37
msgid "Copy Merged"
msgstr "Còpia fusionada"

#: ../../reference_manual/main_menu/edit_menu.rst:39
msgid "Copies the selection over all layers. Shortcut: :kbd:`Ctrl + Shift + C`"
msgstr ""
"Copia la selecció sobre totes les capes. Drecera: :kbd:`Ctrl + Majús. + C`"

#: ../../reference_manual/main_menu/edit_menu.rst:42
msgid ""
"Pastes the copied buffer into the image as a new layer. Shortcut: :kbd:`Ctrl "
"+ V`"
msgstr ""
"Enganxa la memòria intermèdia copiada a la imatge com a una capa nova. "
"Drecera: :kbd:`Ctrl + V`"

#: ../../reference_manual/main_menu/edit_menu.rst:43
msgid "Paste at Cursor"
msgstr "Enganxa en el cursor"

#: ../../reference_manual/main_menu/edit_menu.rst:45
msgid "Same as :term:`paste`, but aligns the image to the cursor."
msgstr "Igual que :term:`enganxa`, però alinea la imatge amb el cursor."

#: ../../reference_manual/main_menu/edit_menu.rst:46
msgid "Paste into new image"
msgstr "Enganxa en una imatge nova"

#: ../../reference_manual/main_menu/edit_menu.rst:48
msgid "Pastes the copied buffer into a new image."
msgstr "Enganxa la memòria intermèdia copiada a una imatge nova."

#: ../../reference_manual/main_menu/edit_menu.rst:49
msgid "Clear"
msgstr "Neteja"

#: ../../reference_manual/main_menu/edit_menu.rst:51
msgid "Clear the current layer. Shortcut: :kbd:`Del`"
msgstr "Neteja la capa actual. Drecera: :kbd:`Supr`"

#: ../../reference_manual/main_menu/edit_menu.rst:52
msgid "Fill with Foreground Color"
msgstr "Emplena amb el color de primer pla"

#: ../../reference_manual/main_menu/edit_menu.rst:54
msgid ""
"Fills the layer or selection with the foreground color. Shortcut: :kbd:"
"`Shift + Backspace`"
msgstr ""
"Emplena la capa o selecció amb el color de primer pla. Drecera: :kbd:`Majús. "
"+ Retrocés`"

#: ../../reference_manual/main_menu/edit_menu.rst:55
msgid "Fill with Background Color"
msgstr "Emplena amb el color de fons"

#: ../../reference_manual/main_menu/edit_menu.rst:57
msgid ""
"Fills the layer or selection with the background color. Shortcut: :kbd:"
"`Backspace`"
msgstr ""
"Emplena la capa o selecció amb el color de fons. Drecera: :kbd:`Retrocés`"

#: ../../reference_manual/main_menu/edit_menu.rst:58
msgid "Fill with pattern"
msgstr "Emplena amb un patró"

#: ../../reference_manual/main_menu/edit_menu.rst:60
msgid "Fills the layer or selection with the active pattern."
msgstr "Emplena la capa o selecció amb el patró actiu."

#: ../../reference_manual/main_menu/edit_menu.rst:61
msgid "Stroke Selected Shapes"
msgstr "Traça les formes seleccionades"

#: ../../reference_manual/main_menu/edit_menu.rst:63
msgid ""
"Strokes the selected vector shape with the selected brush, will create a new "
"layer."
msgstr ""
"Traça la forma vectorial seleccionada amb el pinzell seleccionat, crearà una "
"capa nova."

#: ../../reference_manual/main_menu/edit_menu.rst:64
msgid "Stroke Selection"
msgstr "Selecciona amb el traç"

#: ../../reference_manual/main_menu/edit_menu.rst:66
msgid "Strokes the active selection using the menu."
msgstr "Traça la selecció activa utilitzant el menú."
