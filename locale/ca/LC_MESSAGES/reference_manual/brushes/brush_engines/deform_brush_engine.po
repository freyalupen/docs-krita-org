# Translation of docs_krita_org_reference_manual___brushes___brush_engines___deform_brush_engine.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-08-25 14:46+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<generated>:1
msgid "Use Undeformed Image"
msgstr "Usa la imatge no deformada"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:1
msgid "The Deform Brush Engine manual page."
msgstr "La pàgina del manual per al motor del pinzell de deformació."

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:16
msgid "Deform Brush Engine"
msgstr "Motor del pinzell de deformació"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:11
msgid "Brush Engine"
msgstr "Motor de pinzell"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:11
msgid "Deform"
msgstr "Deforma"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:11
msgid "Liquify"
msgstr "Liqüescent"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:19
msgid ".. image:: images/icons/deformbrush.svg"
msgstr ".. image:: images/icons/deformbrush.svg"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:20
msgid ""
"The Deform Brush is a brush that allows you to pull and push pixels around. "
"It's quite similar to the :ref:`liquify_mode`, but where liquify has higher "
"quality, the deform brush has the speed."
msgstr ""
"El Pinzell de deformació és un pinzell que permet tirar i empènyer dels "
"píxels que hi ha al voltant. És força similar al mode :ref:`liquify_mode`, "
"però on el liqüescent té millor qualitat, el pinzell de deformació té la "
"velocitat."

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:24
msgid "Options"
msgstr "Opcions"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:26
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:27
msgid ":ref:`option_deform`"
msgstr ":ref:`option_deform`"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:28
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:29
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:30
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:31
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:32
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:38
msgid "Deform Options"
msgstr "Opcions de la deformació"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:42
msgid ".. image:: images/brushes/Krita_deform_brush_examples.png"
msgstr ".. image:: images/brushes/Krita_deform_brush_examples.png"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:42
msgid ""
"1: undeformed, 2: Move, 3: Grow, 4: Shrink, 5: Swirl Counter Clock Wise, 6: "
"Swirl Clockwise, 7: Lens Zoom In, 8: Lens Zoom Out"
msgstr ""
"1: Sense deformar, 2: Mou, 3: Fes créixer, 4: Fes encongir, 5: Remolí en el "
"sentit antihorari, 6: Remolí en el sentit horari, 7: Apropa el zoom de la "
"lent, 8: Allunya el zoom de la lent"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:44
msgid "These decide what strangeness may happen underneath your brush cursor."
msgstr ""
"Això decideix quin serà l'efecte estrany que succeirà sota el cursor del "
"vostre pinzell."

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:46
msgid "Grow"
msgstr "Fes créixer"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:47
msgid "This bubbles up the area underneath the brush-cursor."
msgstr "Genera una bombolla a l'àrea que es troba sota el cursor del pinzell."

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:48
msgid "Shrink"
msgstr "Fes encongir"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:49
msgid "This pinches the Area underneath the brush-cursor."
msgstr "Pessiga l'àrea que es troba sota el cursor del pinzell."

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:50
msgid "Swirl Counter Clock Wise"
msgstr "Remolí en el sentit antihorari"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:51
msgid "Swirls the area counter clock wise."
msgstr "Genera un remolí en el sentit antihorari sobre l'àrea."

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:52
msgid "Swirl Clock Wise"
msgstr "Remolí en el sentit horari"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:53
msgid "Swirls the area clockwise."
msgstr "Genera un remolí en el sentit horari sobre l'àrea."

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:54
msgid "Move"
msgstr "Mou"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:55
msgid "Nudges the area to the painting direction."
msgstr "Empeny l'àrea cap a la direcció de la pintura."

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:56
msgid "Color Deformation"
msgstr "Deformació del color"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:57
msgid "This seems to randomly rearrange the pixels underneath the brush."
msgstr ""
"Sembla que torna a organitzar de forma aleatòria els píxels que es troben "
"sota el cursor del pinzell."

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:58
msgid "Lens Zoom In"
msgstr "Apropa el zoom de la lent"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:59
msgid "Literally paints a enlarged version of the area."
msgstr "Pinta literalment una versió allargada de l'àrea."

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:61
msgid "Lens Zoom Out"
msgstr "Allunya el zoom de la lent"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:61
msgid "Paints a minimized version of the area."
msgstr "Pinta una versió minimitzada de l'àrea."

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:65
msgid ".. image:: images/brushes/Krita_deform_brush_colordeform.png"
msgstr ".. image:: images/brushes/Krita_deform_brush_colordeform.png"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:65
msgid "Showing color deform."
msgstr "Demostració de la deformació del color."

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:68
msgid "Deform Amount"
msgstr "Quantitat de deformació"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:68
msgid "Defines the strength of the deformation."
msgstr "Defineix la intensitat de la deformació."

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:72
msgid ".. image:: images/brushes/Krita_deform_brush_bilinear.png"
msgstr ".. image:: images/brushes/Krita_deform_brush_bilinear.png"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:72
#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:74
msgid "Bilinear Interpolation"
msgstr "Interpolació bilineal"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:75
msgid "Smoothens the result. This causes calculation errors in 16bit."
msgstr "Suavitza el resultat. Això causa errors de càlcul en els 16 bits."

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:77
msgid "Use Counter"
msgstr "Usa el comptador"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:77
msgid "Slows down the deformation subtlety."
msgstr "Desaccelera de forma subtil la deformació."

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:81
msgid ".. image:: images/brushes/Krita_deform_brush_useundeformed.png"
msgstr ".. image:: images/brushes/Krita_deform_brush_useundeformed.png"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:81
msgid "Without 'use undeformed' to the left and with to the right."
msgstr ""
"Sense l'opció «Usa la imatge no deformada» a l'esquerra i amb ella a la "
"dreta."

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:84
msgid ""
"Samples from the previous version of the image instead of the current. This "
"works better with some deform options than others. Move for example seems to "
"almost stop working, but it works really well with Grow."
msgstr ""
"Crea mostres de la versió anterior de la imatge en lloc de l'actual. Això "
"funciona millor amb algunes opcions de la deformació que amb altres. Amb "
"l'opció Mou, per exemple, sembla que deixa de funcionar, però resulta "
"realment bé amb l'opció Fes créixer."
