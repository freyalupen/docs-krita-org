# Translation of docs_krita_org_reference_manual___brushes___brush_engines___chalk_engine.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-30 11:07+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:1
msgid "The Chalk Brush Engine manual page."
msgstr "La pàgina del manual per al motor del pinzell de guix."

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:17
msgid "Chalk Brush Engine"
msgstr "Motor del pinzell de guix"

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:21
msgid ""
"This brush engine has been removed in 4.0. There are other brush engines "
"such as pixel that can do everything this can...plus more."
msgstr ""
"Aquest motor de pinzell s'ha eliminat a la versió 4.0. Hi ha altres motors "
"de pinzell, com el de píxels, que aconsegueixen fer tot el que feia "
"aquest... i encara més."

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:23
msgid ""
"Apparently, the Bristle brush engine is derived from this brush engine. Now, "
"all of :program:`Krita's` brushes have a great variety of uses, so you must "
"have tried out the Chalk brush and wondered what it is for. Is it nothing "
"but a pixel brush with opacity and saturation fade options? As per the "
"developers this brush uses a different algorithm than the Pixel Brush, and "
"they left it in here as a simple demonstration of the capabilities of :"
"program:`Krita's` brush engines."
msgstr ""
"Aparentment, el motor del pinzell de pèl es deriva d'aquest motor. Ara, tots "
"els pinzells del :program:`Krita` tenen una gran varietat d'usos, de manera "
"que ja hauríeu d'haver experimentat amb el pinzell de guix i preguntar-vos "
"per a què serveix. No és més que un pinzell de píxels amb opcions per a "
"l'esvaïment de l'opacitat i saturació? Des del punt de vista dels "
"programadors, aquest pinzell utilitza un algorisme diferent del Pinzell de "
"píxels i el van deixar aquí com a una demostració senzilla de les capacitats "
"dels motors de pinzell del :program:`Krita`."

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:26
msgid ""
"So there you go, this brush is here for algorithmic demonstration purposes. "
"Don't lose sleep because you can't figure out what it's for, it Really "
"doesn't do much. For the sake of description, here's what it does:"
msgstr ""
"Com a tal, aquí teniu aquest pinzell només amb l'objecte de demostració dels "
"algorismes. No perdeu el son per no poder percebre per a què serveix, ja que "
"de fet no fa gaire. Només per la descripció, aquí teniu el que fa:"

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:29
msgid ".. image:: images/brushes/Krita-tutorial7-C.png"
msgstr ".. image:: images/brushes/Krita-tutorial7-C.png"

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:30
msgid ""
"Yeah, that's it, a round brush with some chalky texture, and the option to "
"fade in opacity and saturation. That's it."
msgstr ""
"Sí, és així, un pinzell rodó amb alguna textura de guix, així com l'opció "
"per esvair en l'opacitat i la saturació. Això és tot."
