# Translation of docs_krita_org_user_manual___templates.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: user_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 16:48+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../user_manual/templates.rst:1
msgid "How to use document templates in Krita."
msgstr "Com s'utilitzen les plantilles de document en el Krita."

#: ../../user_manual/templates.rst:12
msgid "Template"
msgstr "Plantilla"

#: ../../user_manual/templates.rst:17
msgid "Templates"
msgstr "Plantilles"

#: ../../user_manual/templates.rst:20
msgid ".. image:: images/Krita_New_File_Template_A.png"
msgstr ".. image:: images/Krita_New_File_Template_A.png"

#: ../../user_manual/templates.rst:21
msgid ""
"Templates are just .kra files which are saved in a special location so it "
"can be pulled up by Krita quickly. This is like the :guilabel:`Open Existing "
"Document and Untitled Document` but then with a nicer place in the UI."
msgstr ""
"Les plantilles només són fitxers .kra que es desen en una ubicació especial "
"per tal que el Krita pugui arrencar ràpidament. Això és com :guilabel:`Obre "
"un document existent com a un document sense títol` però amb un lloc millor "
"a la interfície d'usuari."

#: ../../user_manual/templates.rst:23
msgid ""
"You can make your own template file from any .kra file, by using :guilabel:"
"`create template from image` in the file menu. This will add your current "
"document as a new template, including all its properties along with the "
"layers and layer contents."
msgstr ""
"Podeu crear el vostre propi fitxer de plantilla des de qualsevol fitxer ."
"kra, emprant :guilabel:`Crea una plantilla des de la imatge` des del menú "
"Fitxer. Això afegirà el document actual com a una plantilla nova, incloent "
"totes les seves propietats juntament amb les capes i el contingut de la capa."

#: ../../user_manual/templates.rst:25
msgid "We have the following defaults:"
msgstr "Tenim els valors predeterminats següents:"

#: ../../user_manual/templates.rst:28
msgid "Comic Templates"
msgstr "Plantilles per a còmics"

#: ../../user_manual/templates.rst:30
msgid ""
"These templates are specifically designed for you to just get started with "
"drawing comics. The comic template relies on a system of vectors and clones "
"of those vector layers which automatically reflect any changes made to the "
"vector layers. In between these two, you can draw your picture, and not fear "
"them drawing over the panel. Use :guilabel:`Inherit Alpha` to clip the "
"drawing by the panel."
msgstr ""
"Aquestes plantilles han estat dissenyades específicament perquè pugueu "
"començar a dibuixar còmics. La plantilla de còmic es basa en un sistema "
"vectorial i clons d'aquestes capes vectorials, les quals reflecteixen "
"automàticament els canvis realitzats a les capes vectorials. Entre aquests "
"dos, podreu dibuixar la vostra imatge sense por de dibuixar sobre la "
"vinyeta. Utilitzeu :guilabel:`Hereta l'alfa` per a escurçar el dibuix per la "
"vinyeta."

#: ../../user_manual/templates.rst:32
msgid "European Bande Desinée Template."
msgstr "Plantilla per a còmics d'estil bandes Dessinée europeu."

#: ../../user_manual/templates.rst:33
msgid ""
"This one is reminiscent of the system used by for example TinTin or Spirou "
"et Fantasio. These panels focus on wide images, and horizontal cuts."
msgstr ""
"Aquesta és una reminiscència del sistema utilitzat per exemple per a TinTin "
"o Spirou et Fantasio. Aquestes vinyetes se centren en imatges àmplies i "
"retalls horitzontals."

#: ../../user_manual/templates.rst:34
msgid "US-style comics Template."
msgstr "Plantilla per a còmics d'estil americà."

#: ../../user_manual/templates.rst:35
msgid ""
"This one is reminiscent of old DC and Marvel comics, such as Batman or "
"Captain America. Nine images for quick story progression."
msgstr ""
"Aquesta és una reminiscència de antics còmics de DC i Marvel, com Batman o "
"Capità Amèrica. Nou imatges per a una progressió ràpida de la història."

#: ../../user_manual/templates.rst:36
msgid "Manga Template."
msgstr "Plantilla per a manga."

#: ../../user_manual/templates.rst:37
msgid ""
"This one is based on Japanese comics, and focuses on a thin vertical gutter "
"and a thick horizontal gutter, ensuring that the reader finished the "
"previous row before heading to the next."
msgstr ""
"Aquesta es basa en els còmics japonesos, i se centra en un canal vertical "
"prim i un canal horitzontal gruixut, el qual garanteix que el lector acabi "
"de veure la fila anterior abans de passar a la següent."

#: ../../user_manual/templates.rst:39
msgid "Waffle Iron Grid"
msgstr "Reixa de ferro"

#: ../../user_manual/templates.rst:39
msgid "12 little panels at your disposal."
msgstr "12 vinyetes petites a la vostra disposició."

#: ../../user_manual/templates.rst:42
msgid "Design Templates"
msgstr "Plantilles per al disseny"

#: ../../user_manual/templates.rst:44
msgid ""
"These are templates for design and have various defaults with proper ppi at "
"your disposal:"
msgstr ""
"Aquestes són plantilles per al disseny i tenen diversos valors "
"predeterminats amb un PPP (ppi) adequat a la vostra disposició:"

#: ../../user_manual/templates.rst:46
msgid "Cinema 16:10"
msgstr "Cine 16:10"

#: ../../user_manual/templates.rst:47
msgid "Cinema 2.93:1"
msgstr "Cine 2.93:1"

#: ../../user_manual/templates.rst:48
msgid "Presentation A3-landscape"
msgstr "Presentació A3 apaïsada"

#: ../../user_manual/templates.rst:49
msgid "Presentation A4 portrait"
msgstr "Presentació A4 vertical"

#: ../../user_manual/templates.rst:50
msgid "Screen 4:3"
msgstr "Pantalla 4:3"

#: ../../user_manual/templates.rst:51
msgid "Web Design"
msgstr "Disseny web"

#: ../../user_manual/templates.rst:54
msgid "DSLR templates"
msgstr "Plantilles per a rèflex digitals"

#: ../../user_manual/templates.rst:56
msgid "These have some default size for photos"
msgstr "Aquestes tenen alguna mida predeterminada per a les fotografies."

#: ../../user_manual/templates.rst:58
msgid "Canon 55D"
msgstr "Canon 55D"

#: ../../user_manual/templates.rst:59
msgid "Canon 5DMK3"
msgstr "Canon 5DMK3"

#: ../../user_manual/templates.rst:60
msgid "Nikon D3000"
msgstr "Nikon D3000"

#: ../../user_manual/templates.rst:61
msgid "Nikon D5000"
msgstr "Nikon D5000"

#: ../../user_manual/templates.rst:62
msgid "Nikon D7000"
msgstr "Nikon D7000"

#: ../../user_manual/templates.rst:65
msgid "Texture Templates"
msgstr "Plantilles per a textures"

#: ../../user_manual/templates.rst:67
msgid "These are for making 3D textures, and are between 1024, to 4092."
msgstr ""
"Aquestes són per a crear textures en 3D, i estan entre els 1024 fins als "
"4092."
