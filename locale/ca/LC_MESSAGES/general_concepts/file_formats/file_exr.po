# Translation of docs_krita_org_general_concepts___file_formats___file_exr.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: general_concepts\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 13:39+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../general_concepts/file_formats/file_exr.rst:1
msgid "The EXR file format as exported by Krita."
msgstr "El format de fitxer EXR tal com l'exporta el Krita."

#: ../../general_concepts/file_formats/file_exr.rst:10
msgid "EXR"
msgstr "EXR"

#: ../../general_concepts/file_formats/file_exr.rst:10
msgid "HDR Fileformat"
msgstr "Format de fitxer en HDR"

#: ../../general_concepts/file_formats/file_exr.rst:10
msgid "OpenEXR"
msgstr "OpenEXR"

#: ../../general_concepts/file_formats/file_exr.rst:10
msgid "*.exr"
msgstr "*.exr"

#: ../../general_concepts/file_formats/file_exr.rst:15
msgid "\\*.exr"
msgstr "\\*.exr"

# skip-rule: t-acc_obe
#: ../../general_concepts/file_formats/file_exr.rst:17
msgid ""
"``.exr`` is the prime file format for saving and loading :ref:`floating "
"point bit depths <bit_depth>`, and due to the library made to load and save "
"these images being fully open source, the main interchange format as well."
msgstr ""
"L'``.exr`` és el principal format de fitxer per a desar i carregar les :ref:"
"`profunditats de bits de coma flotant <bit_depth>`, i a causa de la "
"biblioteca que s'ha creat per a carregar i desar aquestes imatges, la qual "
"és completament de codi obert, el principal format d'intercanvi també ho és."

#: ../../general_concepts/file_formats/file_exr.rst:19
msgid ""
"Floating point bit-depths are used by the computer graphics industry to "
"record scene referred values, which can be made via a camera or a computer "
"renderer. Scene referred values means that the file can have values whiter "
"than white, which in turn means that such a file can record lighting "
"conditions, such as sunsets very accurately. These EXR files can then be "
"used inside a renderer to create realistic lighting."
msgstr ""
"La indústria de gràfics per ordinador utilitza les profunditats de bits de "
"coma flotant per enregistrar els valors en referència a l'escena, els quals "
"es poden crear a través d'una càmera o un renderitzador a l'ordinador. "
"Valors en referència a l'escena vol dir que el fitxer pot tenir valors més "
"blancs que el blanc, la qual cosa vol dir que aquest fitxer pot enregistrar "
"les condicions de la llum amb molta precisió, com ara les postes de sol. "
"Aquests fitxers EXR es poden utilitzar dins d'un renderitzador per a crear "
"una il·luminació realista."

#: ../../general_concepts/file_formats/file_exr.rst:21
msgid ""
"Krita can load and save EXR for the purpose of paint-over (yes, Krita can "
"paint with scene referred values) and interchange with applications like "
"Blender, Mari, Nuke and Natron."
msgstr ""
"El Krita pot carregar i desar els EXR amb el propòsit de pintar (sí, el "
"Krita pot pintar amb valors en referència a l'escena) i intercanviar-los amb "
"aplicacions com el Blender, Mari, Nuke i Natron."
