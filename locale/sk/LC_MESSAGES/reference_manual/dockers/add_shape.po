# translation of docs_krita_org_reference_manual___dockers___add_shape.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___dockers___add_shape\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-02 09:31+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/dockers/add_shape.rst:1
msgid "The add shape docker."
msgstr "Panel na pridanie tvaru."

#: ../../reference_manual/dockers/add_shape.rst:15
msgid "Add Shape"
msgstr "Pridať tvar"

#: ../../reference_manual/dockers/add_shape.rst:18
#, fuzzy
#| msgid ".. image:: images/en/Krita_Add_Shape_Docker.png"
msgid ".. image:: images/dockers/Krita_Add_Shape_Docker.png"
msgstr ".. image:: images/en/Krita_Add_Shape_Docker.png"

#: ../../reference_manual/dockers/add_shape.rst:19
msgid "A docker for adding KOffice shapes to a Vector Layers."
msgstr ""

#: ../../reference_manual/dockers/add_shape.rst:23
msgid "This got removed in 4.0, the :ref:`vector_library_docker` replacing it."
msgstr ""
