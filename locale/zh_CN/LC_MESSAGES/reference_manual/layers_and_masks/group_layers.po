msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___layers_and_masks___group_layers.pot\n"

#: ../../reference_manual/layers_and_masks/group_layers.rst:1
msgid "How to use group layers in Krita."
msgstr ""

#: ../../reference_manual/layers_and_masks/group_layers.rst:12
msgid "Layers"
msgstr ""

#: ../../reference_manual/layers_and_masks/group_layers.rst:12
msgid "Groups"
msgstr ""

#: ../../reference_manual/layers_and_masks/group_layers.rst:12
msgid "Passthrough Mode"
msgstr ""

#: ../../reference_manual/layers_and_masks/group_layers.rst:17
msgid "Group Layers"
msgstr "分组图层"

#: ../../reference_manual/layers_and_masks/group_layers.rst:19
msgid ""
"While working in complex artwork you'll often find the need to group the "
"layers or some portions and elements of the artwork in one unit. Group "
"layers come in handy for this, they allow you to make a segregate the "
"layers, so you can hide these quickly, or so you can apply a mask to all the "
"layers inside this group as if they are one, you can also recursively "
"transform the content of the group... Just drag the mask so it moves to the "
"layer. They are quickly made with the :kbd:`Ctrl + G` shortcut."
msgstr ""

#: ../../reference_manual/layers_and_masks/group_layers.rst:21
msgid ""
"A thing to note is that the layers inside a group layer are considered "
"separately when the layer gets composited, the layers inside a group are "
"separately composited and then this image is taken in to account when "
"compositing the whole image, while on the contrary, the groups in Photoshop "
"have something called pass-through mode which makes the layer behave as if "
"they are not in a group and get composited along with other layers of the "
"stack. The recent versions of Krita have pass-through mode you can enable it "
"to get similar behavior"
msgstr ""
