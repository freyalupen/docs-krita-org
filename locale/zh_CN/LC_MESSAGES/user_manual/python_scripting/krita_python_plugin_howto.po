msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-12 03:19+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_user_manual___python_scripting___krita_python_plugin_howto."
"pot\n"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:1
msgid "Guide on all the specifics of creating Krita python plugins."
msgstr "构建 Krita Python 插件的技术指引。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:14
msgid "Python"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:14
msgid "Python Scripting"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:14
msgid "Scripting"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:14
msgid "Plugin"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:19
msgid "How to make a Krita Python plugin"
msgstr "Krita Python 插件编写教程"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:21
msgid ""
"You might have some neat scripts you have written in the Scripter Python "
"runner, but maybe you want to do more with it and run it automatically for "
"instance. Wrapping your script in a plugin can give you much more "
"flexibility and power than running scripts from the Scripter editor."
msgstr ""
"你可能已经编写了一些可以在 Python 脚本工具中运行的脚本，但你说不定还想进一步"
"扩展其功能，并且让它随 Krita 进程自动启动。把脚本包装成插件不但用起来更方便，"
"功能也更加强大。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:23
msgid ""
"Okay, so even if you know python really well, there are some little details "
"to getting Krita to recognize a python plugin. So this page will give an "
"overview how to create the various types of python script unique to Krita."
msgstr ""
"尽管你可能已经对 Python 了如指掌，但要想 Krita 正确识别你的插件，学习一些细节"
"知识还是很有必要的。本节将介绍如何创建 Krita 专用的几种 Python 脚本。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:25
msgid ""
"These mini-tutorials are written for people with a basic understanding of "
"python, and in such a way to encourage experimentation instead of plainly "
"copy and pasting code, so read the text carefully."
msgstr ""
"本文的范例是为那些对 Python 已经有了初步了解的人们准备的。这些范例和相关的 "
"API 文档鼓励读者大胆地进行实验，而不是盲目地复制粘贴代码，所以请认真阅读它"
"们。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:28
msgid "Getting Krita to recognize your plugin"
msgstr "创建 Krita 能够识别的插件"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:30
msgid ""
"A script in Krita has two components - the script directory (holding your "
"script's Python files) and a \".desktop\" file that Krita uses to load and "
"register your script. For Krita to load your script both of these must put "
"be in the pykrita subdirectory of your Krita resources folder (on Linux  ~/."
"local/share/krita/pykrita). To find your resources folder start Krita and "
"click the :menuselection:`Settings --> Manage Resources` menu item. This "
"will open a dialog box. Click the :guilabel:`Open Resources Folder` button. "
"This should open a file manager on your system at your Krita resources "
"folder. See the `API <https://api.kde.org/extragear-api/graphics-apidocs/"
"krita/libs/libkis/html/index.html>`_ docs under \"Auto starting scripts\".  "
"If there is no pykrita subfolder in the Krita resources directory use your "
"file manager to create one."
msgstr ""
"Krita 的脚本由两部分组成：脚本的文件夹和脚本的\".desktop\" 文件。 脚本的文件"
"夹保存了脚本的 Python 文件，而 desktop 文件则供 Krita 载入和注册该脚本使用。"
"要让 Krita 载入你的脚本，两者必须同时被放置在 Krita 的资源文件夹的 pykrita 子"
"文件夹里面 (Linux 环境下为 ~/.local/share/krita/pykrita)。要显示资源文件夹，"
"可在菜单栏打开 :menuselection:`设置 --> 管理资源` 对话框。点击 :guilabel:`打"
"开资源文件夹` 按钮后，系统自带的资源管理器将会显示 Krita 的资源文件夹。具体操"
"作可参考 `API 文档 <https://api.kde.org/extragear-api/graphics-apidocs/krita/"
"libs/libkis/html/index.html>`_ 的“Auto starting scripts”一节。如果 Krita 的资"
"源文件夹里没有 pykrita 子文件夹，可以用资源管理器新建一个。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:32
msgid ""
"Scripts are identified by a file that ends in a .desktop extension that "
"contain information about the script itself."
msgstr "Krita 通过 .desktop 文件识别插件，该文件包含了脚本的基本信息。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:34
msgid ""
"Therefore, for each proper plugin you will need to create a folder, and a "
"desktop file."
msgstr "所以，你要为每个插件创建一个文件夹和一个 desktop 文件。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:36
msgid "The desktop file should look as follows::"
msgstr "该 desktop 文件的内容格式如下::"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:38
msgid ""
"[Desktop Entry]\n"
"Type=Service\n"
"ServiceTypes=Krita/PythonPlugin\n"
"X-KDE-Library=myplugin\n"
"X-Python-2-Compatible=false\n"
"X-Krita-Manual=myPluginManual.html\n"
"Name=My Own Plugin\n"
"Comment=Our very own plugin."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:47
msgid "Type"
msgstr "Type"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:48
msgid "This should always be service."
msgstr "此项应总是设为 Service (服务)。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:49
msgid "ServiceTypes"
msgstr "ServiceTypes"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:50
msgid "This should always be Krita/PythonPlugin for python plugins."
msgstr "对于 Krita 的 Python 插件，此项应总是被设为 Krita/PythonPlugin。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:51
msgid "X-KDE-Library"
msgstr "X-KDE-Library"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:52
msgid "This should be the name of the plugin folder you just created."
msgstr "此项指定该插件的文件夹名称。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:53
msgid "X-Python-2-Compatible"
msgstr "X-Python-2-Compatible"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:54
msgid ""
"Whether it is python 2 compatible. If Krita was built with python 2 instead "
"of 3 (``-DENABLE_PYTHON_2=ON`` in the cmake configuration), then this plugin "
"will not show up in the list."
msgstr ""
"指定是否兼容 Python 2。例子中此项为 false (否)，如果 Krita 是为 Python 2 而不"
"是 3 进行编译 (cmake 配置参数为 ``-DENABLE_PYTHON_2=ON`` )，那么 false 意味着"
"此插件不会在 Krita 里面显示。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:55
msgid "X-Krita-Manual"
msgstr "X-Krita-Manual"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:56
msgid ""
"An Optional Value that will point to the manual item. This is shown in the "
"Python Plugin manager. If it's `an HTML file it'll be shown as rich text "
"<https://doc.qt.io/qt-5/richtext-html-subset.html>`_, if not, it'll be shown "
"as plain text."
msgstr ""
"此项为可选参数，它指向该插件配套的使用手册项目。它会被显示在 Python 插件管理"
"器中。 `HTML 格式的手册将显示为富文本 <https://doc.qt.io/qt-5/richtext-html-"
"subset.html>`_ ，否则将显示为纯文本。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:57
msgid "Name"
msgstr "Name"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:58
msgid "The name that will show up in the Python Plugin Manager."
msgstr "插件在 Python 插件管理器中显示的名称。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:60
msgid "Comment"
msgstr "Comment"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:60
msgid "The description that will show up in the Python Plugin Manager."
msgstr "插件在 Python 插件管理器中的描述。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:62
msgid ""
"Krita python plugins need to be python modules, so make sure there's an "
"__init__.py script, containing something like..."
msgstr ""
"Krita 的 Python 插件必须是 Python 模块，因此每个插件都要包含一个 __init__.py "
"脚本，该脚本包含下面这行代码："

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:68
#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:89
msgid "from .myplugin import *"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:69
msgid ""
"Where .myplugin is the name of the main file of your plugin. If you restart "
"Krita, it now should show this in the Python Plugin Manager in the settings, "
"but it will be grayed out, because there's no myplugin.py. If you hover over "
"disabled plugins, you can see the error with them."
msgstr ""
".myplugin 字串用来指定插件的主程序文件名。按照上面的介绍制作一个插件放到 "
"Krita 的资源文件夹，然后重新启动 Krita，该插件便会被显示在“配置 Krita”对话框"
"的“Python 插件管理器”页面。不过现在它会被显示为灰色，因为插件的文件夹里面并没"
"有一个叫做 myplugin.py 的文件。如果把鼠标悬停在被停用的插件上，你可以看到相关"
"的错误信息。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:71
msgid ""
"You need to explicitly enable your plugin. Go to the Settings menu, open the "
"Configure Krita dialog and go to the Python Plugin Manager page and enable "
"your plugin."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:74
msgid "Summary"
msgstr "小结"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:76
msgid "In summary, if you want to create a script called *myplugin*:"
msgstr "总而言之，如果你想要创建一个叫做 *myplugin* 的脚本："

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:79
msgid "in your Krita *resources/pykrita* directory create"
msgstr "在 Krita 的资源文件夹的 pykrita 子文件夹里面"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:79
msgid "a folder called *myplugin*"
msgstr "创建一个名为 *myplugin* 的文件夹"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:80
msgid "a file called *myplugin.desktop*"
msgstr "创建一个名为 *myplugin.desktop* 的文件"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:82
msgid "in the *myplugin* folder create"
msgstr "在 *myplugin* 文件夹里面"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:82
msgid "a file called *__init__.py*"
msgstr "创建一个名为 *__init__.py* 的文件"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:83
msgid "a file called *myplugin.py*"
msgstr "创建一个名为 *myplugin.py* 的文件"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:84
msgid "in the *__init__.py* file put this code:"
msgstr "在 *__init__.py* 文件里放置这行代码："

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:90
msgid "in the desktop file put this code::"
msgstr "在 desktop 文件里放置这段代码::"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:92
msgid ""
"[Desktop Entry]\n"
"Type=Service\n"
"ServiceTypes=Krita/PythonPlugin\n"
"X-KDE-Library=myplugin\n"
"X-Python-2-Compatible=false\n"
"Name=My Own Plugin\n"
"Comment=Our very own plugin."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:100
msgid "write your script in the ''myplugin/myplugin.py'' file."
msgstr "在 “myplugin/myplugin.py” 文件里编写脚本代码。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:103
msgid "Creating an extension"
msgstr "创建一个扩展程序"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:105
msgid ""
"`Extensions <https://api.kde.org/extragear-api/graphics-apidocs/krita/libs/"
"libkis/html/classExtension.html>`_ are relatively simple python scripts that "
"run on Krita start. They are made by extending the Extension class, and the "
"most barebones extension looks like this:"
msgstr ""
"`扩展程序 <https://api.kde.org/extragear-api/graphics-apidocs/krita/libs/"
"libkis/html/classExtension.html>`_ 是随 Krita 一起启动的简单 Python 脚本。它"
"们通过 Extension (扩展) 程序类进行编写。一个最简单的扩展程序代码如下："

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:125
msgid ""
"from krita import *\n"
"\n"
"class MyExtension(Extension):\n"
"\n"
"    def __init__(self, parent):\n"
"        #This is initialising the parent, always  important when "
"subclassing.\n"
"        super().__init__(parent)\n"
"\n"
"    def setup(self):\n"
"        pass\n"
"\n"
"    def createActions(self, window):\n"
"        pass\n"
"\n"
"# And add the extension to Krita's list of extensions:\n"
"Krita.instance().addExtension(MyExtension(Krita.instance()))"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:126
msgid ""
"This code of course doesn't do anything. Typically, in createActions we add "
"actions to Krita, so we can access our script from the :guilabel:`Tools` "
"menu."
msgstr ""
"这段代码并不具备任何功能。一般来说我们会在 createActions 处为 Krita 添加功"
"能，这样我们就可以通过 :guilabel:`工具` 菜单来访问该脚本了。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:128
msgid ""
"First, let's create an `action <https://api.kde.org/extragear-api/graphics-"
"apidocs/krita/libs/libkis/html/classAction.html>`_. We can do that easily "
"with `Window.createAction() <https://api.kde.org/extragear-api/graphics-"
"apidocs/krita/libs/libkis/html/classWindow."
"html#a72ec58e53844076c1461966c34a9115c>`_. Krita will call createActions for "
"every Window that is created and pass the right window object that we have "
"to use."
msgstr ""
"首先，让我们创建一个 `操作 <https://api.kde.org/extragear-api/graphics-"
"apidocs/krita/libs/libkis/html/classAction.html>`_ 。我们可以用 `Window."
"createAction() <https://api.kde.org/extragear-api/graphics-apidocs/krita/"
"libs/libkis/html/classWindow.html#a72ec58e53844076c1461966c34a9115c>`_ 来实现"
"它。Krita 会为它创建的每个窗口调用 createActions ，并把需要使用的窗口对象传递"
"过去。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:130
msgid "So..."
msgstr "示例如下："

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:137
msgid ""
"def createActions(self, window):\n"
"    action = window.createAction(\"myAction\", \"My Script\", \"tools/scripts"
"\")"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:138
msgid "\"myAction\""
msgstr "\"myAction\""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:139
msgid ""
"This should be replaced with a unique id that Krita will use to find the "
"action."
msgstr "此项指定 Krita 用来查找该操作的唯一 ID。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:141
msgid "\"My Script\""
msgstr "\"My Script\""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:141
msgid "This is what will be visible in the tools menu."
msgstr "此项为该脚本在工具菜单中的显示名称。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:143
msgid ""
"If you now restart Krita, you will have an action called \"My Script\". It "
"still doesn't do anything, because we haven't connected it to a script."
msgstr ""
"重新启动 Krita 之后，你就会发现在工具菜单的脚本里面多了一个叫做“My Script”的"
"操作了。不过它现在还不能够发挥作用，因为我们还没有把它连接到一个脚本。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:145
msgid ""
"So, let's make a simple export document script. Add the following to the "
"extension class, make sure it is above where you add the extension to Krita:"
msgstr ""
"现在让我们把它写成一个简单的导出文档脚本。把下面的代码添加到 extension 程序"
"类，确保它位于“Krita.instance()”所在的那行代码前面："

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:160
msgid ""
"def exportDocument(self):\n"
"    # Get the document:\n"
"    doc =  Krita.instance().activeDocument()\n"
"    # Saving a non-existent document causes crashes, so lets check for that "
"first.\n"
"    if doc is not None:\n"
"        # This calls up the save dialog. The save dialog returns a tuple.\n"
"        fileName = QFileDialog.getSaveFileName()[0]\n"
"        # And export the document to the fileName location.\n"
"        # InfoObject is a dictionary with specific export options, but when "
"we make an empty one Krita will use the export defaults.\n"
"        doc.exportImage(fileName, InfoObject())"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:161
msgid "And add the import for QFileDialog above with the imports:"
msgstr "然后为上面代码中调用的 QFileDialog 添加 import 功能："

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:167
msgid ""
"from krita import *\n"
"from PyQt5.QtWidgets import QFileDialog"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:168
msgid "Then, to connect the action to the new export document:"
msgstr "最后，把该操作连接到新建导出文档："

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:176
msgid ""
"def createActions(self, window):\n"
"    action = window.createAction(\"myAction\", \"My Script\")\n"
"    action.triggered.connect(self.exportDocument)"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:177
msgid ""
"This is an example of a `signal/slot connection <https://doc.qt.io/qt-5/"
"signalsandslots.html>`_, which Qt applications like Krita use a lot. We'll "
"go over how to make our own signals and slots a bit later."
msgstr ""
"这便是一个 `信号/信号槽连接 <https://doc.qt.io/qt-5/signalsandslots.html>`_ "
"的范例，像 Krita 这样的 Qt 应用程序经常会用到这种连接。我们会在后面介绍如何编"
"写我们自己的信号和信号槽。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:179
msgid "Restart Krita and your new action ought to now export the document."
msgstr "重新启动 Krita 后，你刚刚编写的这个新操作应该就可以导出文档了。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:182
msgid "Creating configurable keyboard shortcuts"
msgstr "创建可配置的键盘快捷键"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:184
msgid ""
"Now, your new action doesn't show up in :menuselection:`Settings --> "
"Configure Krita --> Keyboard Shortcuts`."
msgstr ""
"虽然你的新操作已经可以发挥作用，但是它却并未在菜单栏的 :menuselection:`设置 "
"--> 配置 Krita --> 键盘快捷键` 列表中列出。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:186
msgid ""
"Krita, for various reasons, only adds actions to the shortcuts menu when "
"they are present in an .action file. The action file to get our action to be "
"added to shortcuts should look like this:"
msgstr ""
"Krita 出于某些考虑，只会把提供了 .action 文件的操作添加到快捷键列表。要把我们"
"之前编写的操作添加到快捷键列表，我们需要编写一个 action 文件："

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:210
msgid ""
"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
"<ActionCollection version=\"2\" name=\"Scripts\">\n"
"    <Actions category=\"Scripts\">\n"
"        <text>My Scripts</text>\n"
"\n"
"        <Action name=\"myAction\">\n"
"        <icon></icon>\n"
"        <text>My Script</text>\n"
"        <whatsThis></whatsThis>\n"
"        <toolTip></toolTip>\n"
"        <iconText></iconText>\n"
"        <activationFlags>10000</activationFlags>\n"
"        <activationConditions>0</activationConditions>\n"
"        <shortcut>ctrl+alt+shift+p</shortcut>\n"
"        <isCheckable>false</isCheckable>\n"
"        <statusTip></statusTip>\n"
"        </Action>\n"
"    </Actions>\n"
"</ActionCollection>"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:211
msgid "<text>My Scripts</text>"
msgstr "<text>My Scripts</text>"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:212
msgid ""
"This will create a sub-category under scripts called \"My Scripts\" to add "
"your shortcuts to."
msgstr ""
"此项会在脚本分类下面新建一个名为“My Scripts”的子分类，此操作的快捷键将被添加"
"到该分类。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:213
msgid "name"
msgstr "name"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:214
msgid ""
"This should be the unique id you made for your action when creating it in "
"the setup of the extension."
msgstr "此项填写你在编写该扩展程序时给它命名的唯一 ID。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:215
msgid "icon"
msgstr "icon"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:216
msgid ""
"the name of a possible icon. These will only show up on KDE plasma, because "
"Gnome and Windows users complained they look ugly."
msgstr ""
"此项指定一个可用的图标名称，仅在 KDE Plasma 下面显示，因为 Gnome 和 Windows "
"用户觉得在菜单里显示图标不美观。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:217
msgid "text"
msgstr "text"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:218
msgid "The text that it will show in the shortcut editor."
msgstr "指定在快捷键编辑器中显示的文字。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:219
msgid "whatsThis"
msgstr "whatsThis"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:220
msgid ""
"The text it will show when a Qt application specifically calls for 'what is "
"this', which is a help action."
msgstr "此项指定的文字会在 Qt 应用程序调用“这是什么”帮助信息时显示。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:221
msgid "toolTip"
msgstr "toolTip"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:222
msgid "The tool tip, this will show up on hover-over."
msgstr "指定在鼠标悬停时显示的工具提示文字。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:223
msgid "iconText"
msgstr "iconText"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:224
msgid ""
"The text it will show when displayed in a toolbar. So for example, \"Resize "
"Image to New Size\" could be shortened to \"Resize Image\" to save space, so "
"we'd put that in here."
msgstr ""
"指定在工具栏中显示时的替代文字。例如“缩放图像至新尺寸”可以被缩写为“调整图像大"
"小”以节省工具栏空间。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:225
msgid "activationFlags"
msgstr "activationFlags"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:226
msgid "This determines when an action is disabled or not."
msgstr "此项决定该操作是否被禁用。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:227
msgid "activationConditions"
msgstr "activationConditions"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:228
msgid ""
"This determines activation conditions (e.g. activate only when selection is "
"editable). See `the code <https://cgit.kde.org/krita.git/tree/libs/ui/"
"kis_action.h#n76>`_ for examples."
msgstr ""
"此项决定该操作被激活的条件 (例如仅在选区可编辑时激活)。可以参考 `相关用例代"
"码 <https://cgit.kde.org/krita.git/tree/libs/ui/kis_action.h#n76>`_ 。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:229
msgid "shortcut"
msgstr "shortcut"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:230
msgid "Default shortcut."
msgstr "指定默认的快捷键。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:231
msgid "isCheckable"
msgstr "isCheckable"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:232
msgid "Whether it is a checkbox or not."
msgstr "指定它是否一个复选框。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:234
msgid "statusTip"
msgstr "statusTip"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:234
msgid "The status tip that is displayed on a status bar."
msgstr "指定显示在状态栏的提示文字。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:236
msgid ""
"Save this file as \"myplugin.action\" where myplugin is the name of your "
"plugin. The action file should be saved, not in the pykrita resources "
"folder, but rather in a resources folder named \"actions\". (So, share/"
"pykrita is where the python plugins and desktop files go, and share/actions "
"is where the action files go) Restart Krita. The shortcut should now show up "
"in the shortcut action list."
msgstr ""
"把该文件保存为“myplugin.action”，“myplugin”字串应与你的插件名称相同。要注意的"
"是，action 文件不应该保存在资源文件夹的 pykrita 子文件夹，而应该保存在 "
"actions 子文件夹。(把 Python 插件和 desktop 文件放在 share/pykrita 文件夹，"
"把 action 文件放在 share/actions 文件夹) 重新启动 Krita。现在应该可以在快捷键"
"列表找到该操作了。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:239
msgid "Creating a docker"
msgstr "创建工具面板"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:241
msgid ""
"Creating a custom `docker <https://api.kde.org/extragear-api/graphics-"
"apidocs/krita/libs/libkis/html/classDockWidget.html>`_ is much like creating "
"an extension. Dockers are in some ways a little easier, but they also "
"require more use of widgets. This is the barebones docker code:"
msgstr ""
"创建一个 `工具面板 <https://api.kde.org/extragear-api/graphics-apidocs/krita/"
"libs/libkis/html/classDockWidget.html>`_ 和创建一个扩展程序的做法差不多，工具"
"面板在某些方面更容易编写，但需要用到更多的窗口部件。一个最简单的工具面板代码"
"如下："

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:258
msgid ""
"from PyQt5.QtWidgets import *\n"
"from krita import *\n"
"\n"
"class MyDocker(DockWidget):\n"
"\n"
"    def __init__(self):\n"
"        super().__init__()\n"
"        self.setWindowTitle(\"My Docker\")\n"
"\n"
"    def canvasChanged(self, canvas):\n"
"        pass\n"
"\n"
"Krita.instance().addDockWidgetFactory(DockWidgetFactory(\"myDocker\", "
"DockWidgetFactoryBase.DockRight, MyDocker))"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:259
msgid ""
"The window title is how it will appear in the docker list in Krita. "
"canvasChanged always needs to be present, but you don't have to do anything "
"with it, so hence just 'pass'."
msgstr ""
"代码中的 setWindowTitle (设置窗口标题) 一项用于指定该工具面板在 Krita 的工具"
"面板列表中显示的名称。setWindowTitle 一项不可省略，但你无需用它来做任何事情，"
"所以是“pass”。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:261
msgid "For the addDockWidgetFactory..."
msgstr "在 addDockWidgetFactory 里面："

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:263
msgid "\"myDocker\""
msgstr "\"myDocker\""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:264
msgid ""
"Replace this with an unique ID for your docker that Krita uses to keep track "
"of it."
msgstr "把此项替换为你的工具面板的唯一 ID，Krita 使用该 ID 来跟踪此面板。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:265
msgid "DockWidgetFactoryBase.DockRight"
msgstr "DockWidgetFactoryBase.DockRight"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:266
msgid ""
"The location. These can be DockTornOff, DockTop, DockBottom, DockRight, "
"DockLeft, or DockMinimized"
msgstr ""
"此项指定面板的位置，可以是 DockTornOff、DockTop、DockBottom、DockRight、"
"DockLeft 或者 DockMinimized。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:268
msgid "MyDocker"
msgstr "MyDocker"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:268
msgid "Replace this with the class name of the docker you want to add."
msgstr "把此项替换为你想要添加的工具面板的程序类名称。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:270
msgid ""
"So, if we add our export document function we created in the extension "
"section to this docker code, how do we allow the user to activate it? First, "
"we'll need to do some Qt GUI coding: Let's add a button!"
msgstr ""
"如果我们把之前编写的导出文档扩展程序添加到这个工具面板，用户要怎样才能激活它"
"呢？答案当然就是用“按钮”了！要添加一个按钮，我们需要进行一些 Qt GUI 编程。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:272
msgid ""
"By default, Krita uses PyQt, but its documentation is pretty bad, mostly "
"because the regular Qt documentation is really good, and you'll often find "
"that the PyQT documentation of a class, say, `QWidget <https://www."
"riverbankcomputing.com/static/Docs/PyQt4/qwidget.html>`_ is like a weird "
"copy of the regular `Qt documentation <https://doc.qt.io/qt-5/qwidget."
"html>`_ for that class."
msgstr ""
"Krita 默认使用 PyQt，但它的文档非常差劲。这大概是因为常规的 Qt 文档非常完善，"
"而且也把 PyQt 作为程序类包含在内了，所以 PyQt 项目就懒得写自己的文档了。例如 "
"PyQt 文档的 `QWidget 章节 <https://www.riverbankcomputing.com/static/Docs/"
"PyQt4/qwidget.html>`_ 看上去就是 `常规 Qt 文档 <https://doc.qt.io/qt-5/"
"qwidget.html>`_ 给该程序类编写的文档的翻版。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:274
msgid ""
"Anyway, what we need to do first is that we need to create a QWidget, it's "
"not very complicated, under setWindowTitle, add:"
msgstr ""
"不管怎样，我们首先要做的是创建一个 QWidget，这并不是非常复杂，在 "
"setWindowTitle 下面添加："

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:280
msgid ""
"mainWidget = QWidget(self)\n"
"self.setWidget(mainWidget)"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:281
msgid "Then, we create a button:"
msgstr "然后创建一个按钮："

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:286
msgid "buttonExportDocument = QPushButton(\"Export Document\", mainWidget)"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:287
msgid ""
"Now, to connect the button to our function, we'll need to look at the "
"signals in the documentation. `QPushButton <https://doc.qt.io/qt-5/"
"qpushbutton.html>`_ has no unique signals of its own, but it does say it "
"inherits 4 signals from `QAbstractButton <https://doc.qt.io/qt-5/"
"qabstractbutton.html#signals>`_, which means that we can use those too. In "
"our case, we want clicked."
msgstr ""
"要把这个按钮连接到我们编写的功能，首先要文档里查找它的信号。 `QPushButton "
"<https://doc.qt.io/qt-5/qpushbutton.html>`_ 并不具备它自己特有的信号，但 Qt "
"文档提到它从 `QAbstractButton <https://doc.qt.io/qt-5/qabstractbutton."
"html#signals>`_ 那里继承了 4 个信号，所以我们可以使用那些信号。在这个例子里我"
"们将使用 clicked 信号。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:292
msgid "buttonExportDocument.clicked.connect(self.exportDocument)"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:293
msgid ""
"If we now restart Krita, we'll have a new docker and in that docker there's "
"a button. Clicking on the button will call up the export function."
msgstr ""
"重新启动 Krita 之后我们就有了一个工具面板，面板上还有一个按钮。点击按钮就会调"
"用我们编写的导出功能。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:295
msgid ""
"However, the button looks aligned a bit oddly. That's because our mainWidget "
"has no layout. Let's quickly do that:"
msgstr ""
"不过这个按钮在界面里的位置有点不舒服。这是因为我们的 mainWidget 部件没有布"
"局。让我们完成这一步："

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:301
msgid ""
"mainWidget.setLayout(QVBoxLayout())\n"
"mainWidget.layout().addWidget(buttonExportDocument)"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:302
msgid ""
"Qt has several `layouts <https://doc.qt.io/qt-5/qlayout.html>`_, but the "
"`QHBoxLayout and the QVBoxLayout <https://doc.qt.io/qt-5/qboxlayout.html>`_ "
"are the easiest to use, they just arrange widgets horizontally or vertically."
msgstr ""
"Qt 有好几种 `布局方式 <https://doc.qt.io/qt-5/qlayout.html>`_ ，但 "
"`QHBoxLayout 和 QVBoxLayout <https://doc.qt.io/qt-5/qboxlayout.html>`_ 最容易"
"使用，它们只是按水平和垂直位置排列窗口部件。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:304
msgid "Restart Krita and the button should now be laid out nicely."
msgstr "重新启动 Krita 之后，按钮的位置应该就好看的多了。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:307
msgid "PyQt Signals and Slots"
msgstr "创建 PyQt 信号和信号槽"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:309
msgid ""
"We've already been using PyQt signals and slots already, but there are times "
"where you want to create your own signals and slots. `As pyQt's "
"documentation is pretty difficult to understand <https://www."
"riverbankcomputing.com/static/Docs/PyQt4/new_style_signals_slots.html>`_, "
"and the way how signals and slots are created is very different from C++ Qt, "
"we're explaining it here:"
msgstr ""
"我们在刚才的范例中已经使用过 PyQt 信号和信号槽，但有时候你也需要创建自己的信"
"号和信号槽。不过 `因为 PyQt 的文档不知所云 <https://www.riverbankcomputing."
"com/static/Docs/PyQt4/new_style_signals_slots.html>`_ ，然后信号和信号槽的创"
"建方式又跟 C++ 下的 Qt 很不一样，因此我们在此展开说明一下："

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:312
msgid ""
"All python functions you make in PyQt can be understood as slots, meaning "
"that they can be connected to signals like Action.triggered or QPushButton."
"clicked. However, QCheckBox has a signal for toggled, which sends a boolean. "
"How do we get our function to accept that boolean?"
msgstr ""
"你使用 PyQt 制作的所有 Python 功能都可以被看作是信号槽。这意味着它们可以接受 "
"Action.triggered 或者 QPushButton.clicked 这样的信号。但 QCheckBox 有一个开关"
"信号，它发送的是布尔值。我们要怎样才能使功能的信号槽接受布尔值呢？"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:314
msgid "First, make sure you have the right import for making custom slots:"
msgstr "首先，你要为自定义信号槽指定合适的 import 功能："

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:316
msgid "``from PyQt5.QtCore import pyqtSlot``"
msgstr "``from PyQt5.QtCore import pyqtSlot``"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:318
msgid ""
"(If there's from ``PyQt5.QtCore import *`` already in the list of imports, "
"then you won't have to do this, of course.)"
msgstr ""
"(当然，如果 ``PyQt5.QtCore import *`` 已经在 import 的列表里，你就没必要再做"
"一遍了。)"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:320
msgid "Then, you need to add a PyQt slot definition before your function:"
msgstr "然后，你要在该功能前面添加一个 PyQt 信号槽定义："

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:331
msgid ""
"@pyqtSlot(bool)\n"
"def myFunction(self, enabled):\n"
"    enabledString = \"disabled\"\n"
"    if (enabled == True):\n"
"        enabledString = \"enabled\"\n"
"    print(\"The checkbox is\"+enabledString)"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:332
msgid ""
"Then, when you have created your checkbox, you can do something like "
"myCheckbox.toggled.connect(self.myFunction)"
msgstr ""
"接下来，在你创建了复选框之后，你可以使用类似下面的这些功能：myCheckbox."
"toggled.connect(self.myFunction)"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:334
msgid "Similarly, to make your own PyQt signals, you do the following:"
msgstr "与之类似，要创建自定义 PyQt 信号，可以如下进行："

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:345
msgid ""
"# signal name is added to the member variables of the class\n"
"signal_name = pyqtSignal(bool, name='signalName')\n"
"\n"
"def emitMySignal(self):\n"
"    # And this is how you trigger the signal to be emitted.\n"
"    self.signal_name.emit(True)"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:346
msgid "And use the right import:"
msgstr "记得指定合适的 import 功能："

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:348
msgid "``from PyQt5.QtCore import pyqtSignal``"
msgstr "``from PyQt5.QtCore import pyqtSignal``"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:350
msgid ""
"To emit or create slots for objects that aren't standard python objects, you "
"only have to put their names between quotation marks."
msgstr ""
"在使用非标准 Python 对象的信息和信息槽时，你要把它们的名称放到引号里面。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:353
msgid "A note on unit tests"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:355
msgid ""
"If you want to write unit tests for your plugin, have a look at the `mock "
"krita module <https://github.com/rbreu/krita-python-mock>`_."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:359
msgid "Conclusion"
msgstr "结语"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:361
msgid ""
"Okay, so that covers all the Krita specific details for creating python "
"plugins. It doesn't handle how to parse the pixel data, or best practices "
"with documents, but if you have a little bit of experience with python you "
"should be able to start creating your own plugins."
msgstr ""
"以上便是给 Krita 创建一个 Python 插件所必须的全部技术细节。虽然本教程既没有介"
"绍如何解析像素数据，也没有介绍处理文档时的最佳做法，但只要你有一些 Python 的"
"编程经验，创建一个插件应该难不倒你。"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:363
msgid ""
"As always, read the code carefully and read the API docs for python, Krita "
"and Qt carefully to see what is possible, and you'll get pretty far."
msgstr ""
"还是那句话，认真阅读为 Krita、Qt 和 Python 准备的 API 文档，大胆探索你可以用"
"它们来做些什么，我们相信你一定能够把 Krita 的 Python 功能运用自如，开拓出一片"
"新天地。"
