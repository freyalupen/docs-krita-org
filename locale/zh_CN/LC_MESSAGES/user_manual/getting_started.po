msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_user_manual___getting_started.pot\n"

#: ../../user_manual/getting_started.rst:5
msgid "Getting Started"
msgstr "入门知识"

#: ../../user_manual/getting_started.rst:7
msgid ""
"Welcome to the Krita Manual! In this section, we'll try to get you up to "
"speed."
msgstr "欢迎使用 Krita 手册！我们将在本章内容带你快速上手 Krita。"

#: ../../user_manual/getting_started.rst:9
msgid ""
"If you are familiar with digital painting, we recommend checking out the :"
"ref:`introduction_from_other_software` category, which contains guides that "
"will help you get familiar with Krita by comparing its functions to other "
"software."
msgstr ""

#: ../../user_manual/getting_started.rst:11
msgid ""
"If you are new to digital art, just start with :ref:`installation`, which "
"deals with installing Krita, and continue on to :ref:`starting_with_krita`, "
"which helps with making a new document and saving it, :ref:`basic_concepts`, "
"in which we'll try to quickly cover the big categories of Krita's "
"functionality, and finally, :ref:`navigation`, which helps you find basic "
"usage help, such as panning, zooming and rotating."
msgstr ""
"如果你刚刚接触数字绘画，可从 :ref:`installation` 开始，该小节介绍了如何安装 "
"Krita。:ref:`starting_with_krita` 小节介绍了如何新建和保存文档。:ref:"
"`basic_concepts` 小节简要介绍了 Krita 各个功能大类。:ref:`navigation` 小节介"
"绍了如何使用 Krita 的界面，如平移、缩放和旋转画布等。"

#: ../../user_manual/getting_started.rst:13
msgid ""
"When you have mastered those, you can look into the dedicated introduction "
"pages for functionality in the :ref:`user_manual`, read through the "
"overarching concepts behind (digital) painting in the :ref:"
"`general_concepts` section, or just search the :ref:`reference_manual` for "
"what a specific button does."
msgstr ""
"在掌握这些基础之后，你便可以进行更深入的学习。:ref:`user_manual` 的其他章节"
"对 Krita 的各个主要功能进行了更为详尽的介绍。:ref:`general_concepts` 一章介绍"
"了数字绘画和绘画本身的一些基本理念。:ref:`reference_manual` 一章可以查找到每"
"个按钮的作用。"

#: ../../user_manual/getting_started.rst:15
msgid "Contents:"
msgstr "目录："
