# Translation of docs_krita_org_reference_manual___preferences___general_settings.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___preferences___general_settings\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-14 03:19+0200\n"
"PO-Revision-Date: 2019-08-14 08:47+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<generated>:1
msgid "Recalculate animation cache in background."
msgstr "Повторно створювати кеш анімації у фоновому режимі."

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Krita_Preferences_General.png"
msgstr ".. image:: images/preferences/Krita_Preferences_General.png"

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Settings_cursor_tool_icon.png"
msgstr ".. image:: images/preferences/Settings_cursor_tool_icon.png"

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Settings_cursor_arrow.png"
msgstr ".. image:: images/preferences/Settings_cursor_arrow.png"

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Settings_cursor_crosshair.png"
msgstr ".. image:: images/preferences/Settings_cursor_crosshair.png"

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Settings_cursor_small_circle.png"
msgstr ".. image:: images/preferences/Settings_cursor_small_circle.png"

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Settings_cursor_no_cursor.png"
msgstr ".. image:: images/preferences/Settings_cursor_no_cursor.png"

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Settings_cursor_triangle_righthanded.png"
msgstr ".. image:: images/preferences/Settings_cursor_triangle_righthanded.png"

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Settings_cursor_triangle_lefthanded.png"
msgstr ".. image:: images/preferences/Settings_cursor_triangle_lefthanded.png"

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Settings_cursor_black_pixel.png"
msgstr ".. image:: images/preferences/Settings_cursor_black_pixel.png"

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Settings_cursor_white_pixel.png"
msgstr ".. image:: images/preferences/Settings_cursor_white_pixel.png"

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Krita_4_0_kinetic_scrolling.gif"
msgstr ".. image:: images/preferences/Krita_4_0_kinetic_scrolling.gif"

#: ../../reference_manual/preferences/general_settings.rst:1
msgid "General Preferences in Krita."
msgstr "Загальні налаштування у Krita."

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "Preferences"
msgstr "Налаштування"

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "Settings"
msgstr "Параметри"

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "Cursor"
msgstr "Курсор"

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "Autosave"
msgstr "Автозбереження"

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "Tabbed Documents"
msgstr "Документи у вкладках"

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "Subwindow Documents"
msgstr "Документи у підвікнах"

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "Pop up palette"
msgstr "Контекстна палітра"

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "File Dialog"
msgstr "Діалогове вікно файлів"

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "Maximum Brush Size"
msgstr "Максимальний розмір пензля"

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "Kinetic Scrolling"
msgstr "Кінетичне гортання"

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "Sessions"
msgstr "Сеанси"

#: ../../reference_manual/preferences/general_settings.rst:17
msgid "General Settings"
msgstr "Загальні параметри"

#: ../../reference_manual/preferences/general_settings.rst:19
msgid ""
"You can access the General Category of the preferences by first going to  :"
"menuselection:`Settings --> Configure Krita`."
msgstr ""
"Доступ до загальної категорії параметрів програми можна отримати за "
"допомогою пункту меню :menuselection:`Параметри --> Налаштувати Krita`."

#: ../../reference_manual/preferences/general_settings.rst:24
msgid "Cursor Settings"
msgstr "Параметра курсора"

#: ../../reference_manual/preferences/general_settings.rst:26
msgid "Customize the drawing cursor here:"
msgstr "Тут можна налаштувати курсор для малювання:"

#: ../../reference_manual/preferences/general_settings.rst:29
msgid "Cursor Shape"
msgstr "Форма курсора"

#: ../../reference_manual/preferences/general_settings.rst:31
msgid ""
"Select a cursor shape to use while the brush tools are used. This cursor "
"will always be visible on the canvas. It is usually set to a type exactly "
"where your pen nib is at. The available cursor types are shown below."
msgstr ""
"Вибрати форму курсора, яку слід використовувати для пензлів. Цей вказівник "
"буде завжди видимим на полотні. Зазвичай, встановлюється відповідно до типу "
"кінчика пера стила. Доступні типи курсорів показано нижче."

#: ../../reference_manual/preferences/general_settings.rst:34
msgid "Shows the currently selected tool icon, even for the freehand brush."
msgstr ""
"Показувати піктограму поточного вибраного інструмента, навіть для пензля "
"довільного малювання."

#: ../../reference_manual/preferences/general_settings.rst:36
msgid "Tool Icon"
msgstr "Піктограма інструмента"

#: ../../reference_manual/preferences/general_settings.rst:39
msgid "Shows a generic cursor."
msgstr "Показувати звичайний курсор."

#: ../../reference_manual/preferences/general_settings.rst:41
msgid "Arrow"
msgstr "Стрілка"

#: ../../reference_manual/preferences/general_settings.rst:44
msgid "Shows a precision reticule."
msgstr "Показати візирні нитки прицілювання."

#: ../../reference_manual/preferences/general_settings.rst:46
msgid "Crosshair"
msgstr "Схрещення"

#: ../../reference_manual/preferences/general_settings.rst:48
msgid "Small circle"
msgstr "Маленький кружок"

#: ../../reference_manual/preferences/general_settings.rst:50
msgid "Shows a small white dot with a black outline."
msgstr "Показати невеличку білу плямку із чорною межею."

#: ../../reference_manual/preferences/general_settings.rst:55
msgid "Show no cursor, useful for tablet-monitors."
msgstr "Не показувати курсор. Зручно для сенсорних моніторів."

#: ../../reference_manual/preferences/general_settings.rst:57
msgid "No Cursor"
msgstr "Без курсора"

#: ../../reference_manual/preferences/general_settings.rst:59
msgid "Triangle Right-Handed."
msgstr "Трикутник із вершиною праворуч."

#: ../../reference_manual/preferences/general_settings.rst:61
msgid "Gives a small white triangle with a black border."
msgstr "Малий білий трикутник із чорною межею."

#: ../../reference_manual/preferences/general_settings.rst:66
msgid "Same as above but mirrored."
msgstr "Те саме, що вище, але віддзеркалене."

#: ../../reference_manual/preferences/general_settings.rst:68
msgid "Triangle Left-Handed."
msgstr "Трикутник із вершиною ліворуч."

#: ../../reference_manual/preferences/general_settings.rst:71
msgid "Gives a single black pixel."
msgstr "Одинарний чорний піксель."

#: ../../reference_manual/preferences/general_settings.rst:73
msgid "Black Pixel"
msgstr "Чорний піксель"

#: ../../reference_manual/preferences/general_settings.rst:76
msgid "Gives a single white pixel."
msgstr "Одинарний білий піксель."

#: ../../reference_manual/preferences/general_settings.rst:79
msgid "White Pixel"
msgstr "Білий піксель"

#: ../../reference_manual/preferences/general_settings.rst:82
msgid "Outline Shape"
msgstr "Форма контуру"

#: ../../reference_manual/preferences/general_settings.rst:84
msgid ""
"Select an outline shape to use while the brush tools are used. This cursor "
"shape will optionally show in the middle of a painting stroke as well. The "
"available outline shape types are shown below. (pictures will come soon)"
msgstr ""
"Виберіть форму контуру, якою слід скористатися для інструментів пензля. Якщо "
"захочете, цю форму курсора буде показано посередині штриха малювання. "
"Доступні форми контурів показано нижче."

#: ../../reference_manual/preferences/general_settings.rst:86
msgid "No Outline"
msgstr "Без контуру"

#: ../../reference_manual/preferences/general_settings.rst:87
msgid "No outline."
msgstr "Без контуру."

#: ../../reference_manual/preferences/general_settings.rst:88
msgid "Circle Outline"
msgstr "Круговий контур"

#: ../../reference_manual/preferences/general_settings.rst:89
msgid "Gives a circular outline approximating the brush size."
msgstr "Додає круговий контур апроксимації розміру пензля."

#: ../../reference_manual/preferences/general_settings.rst:90
msgid "Preview Outline"
msgstr "Контур перегляду"

#: ../../reference_manual/preferences/general_settings.rst:91
msgid "Gives an outline based on the actual shape of the brush."
msgstr "Додає контур на основі справжньої форми пензля."

#: ../../reference_manual/preferences/general_settings.rst:93
msgid "Gives a circular outline with a tilt-indicator."
msgstr "Додає круговий контур із індикатором нахилу."

#: ../../reference_manual/preferences/general_settings.rst:95
msgid "Tilt Outline"
msgstr "Контур нахилу"

#: ../../reference_manual/preferences/general_settings.rst:98
msgid "While Painting..."
msgstr "Під час малювання…"

#: ../../reference_manual/preferences/general_settings.rst:101
msgid ""
"This option when selected will show the brush outline while a stroke is "
"being made. If unchecked the brush outline will not appear during stroke "
"making, it will show up only after the brush stroke is finished. This option "
"works only when Brush Outline is selected as the Cursor Shape."
msgstr ""
"Якщо позначено цей пункт, програма показуватиме контур пензля під час "
"малювання штриха. Якщо пункт не буде позначено, контур не буде показано під "
"час створення штриха — програма покаже його, лише коли створення штриха буде "
"завершено. Цей пункт працюватиме, лише якщо для пункту :guilabel:`Контур "
"пензля` вибрано значення :guilabel:`Форма курсора`."

#: ../../reference_manual/preferences/general_settings.rst:105
msgid "Show Outline"
msgstr "Показати обрис"

#: ../../reference_manual/preferences/general_settings.rst:105
msgid "Used to be called \"Show Outline When Painting\"."
msgstr "Раніше мав назву «Показувати обрис під час малювання»."

#: ../../reference_manual/preferences/general_settings.rst:107
msgid "Use effective outline size"
msgstr "Використовувати ефективний розмір обрису"

#: ../../reference_manual/preferences/general_settings.rst:111
msgid ""
"This makes sure that the outline size will always be the maximum possible "
"brush diameter, and not the current one as affected by sensors such as "
"pressure. This makes the cursor a little less noisy to use."
msgstr ""
"За допомогою цього пункту можна обмежити розмір контуру максимальним "
"можливим діаметром пензля, а не поточним діаметром, на який впливають "
"значення датчиків, зокрема тиску. Використання цього пункту зробить курсор "
"менш шумним."

#: ../../reference_manual/preferences/general_settings.rst:114
msgid ""
"The default cursor color. This is mixed with the canvas image so that it "
"will usually have a contrasting color, but sometimes this mixing does not "
"work. This is usually due driver problems. When that happens, you can "
"configure a more pleasant color here."
msgstr ""
"Типовий колір курсора. Цей колір змішується із кольором полотна зображення "
"так, щоб, зазвичай, дати контрастний результат, але іноді таке змішування не "
"працює. Зазвичай, причиною цього є проблеми із драйвером. Якщо у вас "
"трапляються такі неприємності, за допомогою цього пункту ви можете "
"налаштувати зручний для вас колір."

#: ../../reference_manual/preferences/general_settings.rst:115
msgid "Cursor Color:"
msgstr "Колір курсора:"

#: ../../reference_manual/preferences/general_settings.rst:120
msgid "Window Settings"
msgstr "Параметри вікон"

#: ../../reference_manual/preferences/general_settings.rst:122
msgid "Multiple Document Mode"
msgstr "Режим декількох документів"

#: ../../reference_manual/preferences/general_settings.rst:123
msgid ""
"This can be either tabbed like :program:`GIMP` or :program:`Painttool Sai`, "
"or subwindows, like :program:`Photoshop`."
msgstr ""
"Це може бути інтерфейс із вкладками, подібний до інтерфейсу :program:`GIMP` "
"або :program:`Painttool Sai`, чи із допоміжними вікнами, як у :program:"
"`Photoshop`."

#: ../../reference_manual/preferences/general_settings.rst:124
msgid "Background image"
msgstr "Зображення на тлі"

#: ../../reference_manual/preferences/general_settings.rst:125
msgid "Allows you to set a picture background for subwindow mode."
msgstr "Надає вам змогу встановити тло зображення у режимі із підвікнами."

#: ../../reference_manual/preferences/general_settings.rst:126
msgid "Window Background"
msgstr "Тло вікна"

#: ../../reference_manual/preferences/general_settings.rst:127
msgid "Set the color of the subwindow canvas area."
msgstr "Встановити колір ділянки полотна у підвікні."

#: ../../reference_manual/preferences/general_settings.rst:128
msgid "Don't show contents when moving sub-windows"
msgstr "Не показувати вміст під час руху підвікон."

#: ../../reference_manual/preferences/general_settings.rst:129
msgid ""
"This gives an outline when moving windows to work around ugly glitches with "
"certain graphics-cards."
msgstr ""
"Використовувати контур при переміщенні вікон, щоб запобігти небажаним "
"ефектам при використанні деяких відеокарток."

#: ../../reference_manual/preferences/general_settings.rst:130
msgid "Show on-canvas popup messages"
msgstr "Показувати контекстні повідомлення на полотні"

#: ../../reference_manual/preferences/general_settings.rst:131
msgid ""
"Whether or not you want to see the on-canvas pop-up messages that tell you "
"whether you are in tabbed mode, rotating the canvas, or mirroring it."
msgstr ""
"За допомогою цього пункту можна визначити, чи слід показувати контекстні "
"повідомлення на полотні, які інформуватимуть щодо переходу до режиму з "
"вкладками, обертання полотна або його віддзеркалення."

#: ../../reference_manual/preferences/general_settings.rst:132
msgid "Enable Hi-DPI support"
msgstr "Вмикання підтримки високої роздільності"

#: ../../reference_manual/preferences/general_settings.rst:133
msgid ""
"Attempt to use the Hi-DPI support. It is an option because we are still "
"experiencing bugs on windows."
msgstr ""
"Спробувати скористатися підтримкою екранів із високою роздільною здатністю. "
"Типово цю можливість не увімкнено, оскільки і досі усунено не усі вади у "
"Windows."

#: ../../reference_manual/preferences/general_settings.rst:135
msgid "Allow only one instance of Krita"
msgstr "Дозволяти запускати лише один екземпляр Krita"

#: ../../reference_manual/preferences/general_settings.rst:135
msgid ""
"An instance is a single entry in your system's task manager. Turning this "
"option makes sure that Krita will check if there's an instance of Krita open "
"already when you instruct it to open new documents, and then have your "
"documents opened in that single instance. There's some obscure uses to "
"allowing multiple instances, but if you can't think of any, just keep this "
"option on."
msgstr ""
"Екземпляр — одинарний пункт на панелі керування задачами системи. Якщо буде "
"позначено цей пункт, Krita перевірятиме, чи не запущено вже екземпляр Krita, "
"коли ви наказуватимете програмі відкрити нові документи. Якщо буде виявлено "
"запущений екземпляр, програма відкриватиме усі документи у цьому екземплярі "
"програми. Існує декілька можливих застосувань випадку із одночасним запуском "
"декількох екземплярів програми. Якщо ви не плануєте ними користуватися, "
"просто залиште цей пункт позначеним."

#: ../../reference_manual/preferences/general_settings.rst:140
msgid "Tools Settings"
msgstr "Параметри інструмента"

#: ../../reference_manual/preferences/general_settings.rst:142
msgid "In docker (default)"
msgstr "На бічній панелі (типовий варіант)"

#: ../../reference_manual/preferences/general_settings.rst:143
msgid "Gives you the tool options in a docker."
msgstr "Розташувати параметри інструмента на бічній панелі."

#: ../../reference_manual/preferences/general_settings.rst:145
msgid "In toolbar"
msgstr "На панелі інструментів"

#: ../../reference_manual/preferences/general_settings.rst:145
msgid ""
"Gives you the tool options in the toolbar, next to the brush settings. You "
"can open it with the :kbd:`\\\\` key."
msgstr ""
"Додає параметри інструмента на панель інструментів, поряд із параметрами "
"пензля. Вікно параметрів інструмента можна відкрити за допомогою натискнання "
"клавіші :kbd:`\\\\`."

#: ../../reference_manual/preferences/general_settings.rst:148
msgid "Brush Flow Mode"
msgstr "Режим стікання на пензлі"

#: ../../reference_manual/preferences/general_settings.rst:148
msgid ""
"In Krita 4.2 the behavior of flow in combination with opacity was changed. "
"This allows you to turn it back to the 4.1 behavior. This will however be "
"removed in future versions."
msgstr ""
"У Krita 4.2 було змінено поведінку поєднання потоку фарби із непрозорістю "
"мазка. За допомогою цього пункту ви можете увімкнути поведінку, яку було "
"використано у версії 4.1 програми. Втім, згодом ми плануємо прибрати цей "
"пункт із налаштувань програми."

#: ../../reference_manual/preferences/general_settings.rst:151
msgid "Switch Control/Alt Selection Modifiers"
msgstr "Перемкнути модифікатори позначення Ctrl/Alt"

#: ../../reference_manual/preferences/general_settings.rst:151
msgid ""
"This switches the function of the :kbd:`Ctrl` and :kbd:`Alt` keys when "
"modifying selections. Useful for those used to Gimp instead of Photoshop, or "
"Lefties without a right :kbd:`Alt` key on their keyboard."
msgstr ""
"За допомогою цього пункту можна обміняти призначення клавіш :kbd:`Ctrl` і :"
"kbd:`Alt` при внесенні змін до позначених ділянок. Корисно для тих, хто звик "
"користуватися GIMP, а не Photoshop, або для шульг без правої клавіші :kbd:"
"`Alt` на клавіатурі їхнього комп'ютера."

#: ../../reference_manual/preferences/general_settings.rst:154
msgid "Enable Touchpainting"
msgstr "Увімкнути сенсорне малювання"

#: ../../reference_manual/preferences/general_settings.rst:154
msgid ""
"This allows finger painting with capacitive screens. Some devices have both "
"capacitive touch and a stylus, and then this can interfere. In that case, "
"just toggle this."
msgstr ""
"Уможливлює малювання пальцем на ємнісних сенсорних екранах. На деяких "
"пристроях передбачено одночасно ємнісний сенсорний екран та керування за "
"допомогою стила. Це може бути причиною потенційного конфлікту. Якщо маєте "
"проблеми конфлікту режимів керування, перемкніть цей пункт."

#: ../../reference_manual/preferences/general_settings.rst:156
msgid "Activate transform tool after pasting"
msgstr "Задіяти інструмент «Перетворення» після вставляння"

#: ../../reference_manual/preferences/general_settings.rst:160
msgid ""
"A convenience feature. When enabling this, the transform tool will activate "
"after pasting for quick moving or rotating."
msgstr ""
"Зручна можливість. Якщо буде позначено цей пункт, після вставляння даних "
"програма автоматично вмикатиме інструмент «Перетворення» для швидкого "
"пересування або обертання вставленого."

#: ../../reference_manual/preferences/general_settings.rst:163
msgid "This enables kinetic scrolling for scrollable areas."
msgstr "Вмикає кінетичне гортання у придатних до гортання місцях."

#: ../../reference_manual/preferences/general_settings.rst:168
msgid ""
"Kinetic scrolling on the brush chooser drop-down with activation mode set "
"to :guilabel:`On Click Drag`, with this disabled all of these clicks would "
"lead to a brush being selected regardless of drag motion."
msgstr ""
"Кінетичне гортання у спадному списку вибору пензля із режимом активації, "
"який встановлено у значення :guilabel:`Перетягування з клацанням` із "
"непозначеним цим пунктом призведуть до того, що програма вибиратиме пензель, "
"незалежно від того, що виконується рух з перетягуванням."

#: ../../reference_manual/preferences/general_settings.rst:171
msgid "How it is activated."
msgstr "Спосіб активації."

#: ../../reference_manual/preferences/general_settings.rst:173
msgid "On Middle-Click Drag"
msgstr "Перетягування із затисканням середньої кнопки"

#: ../../reference_manual/preferences/general_settings.rst:174
msgid "Will activate when using the middle mouse button."
msgstr "Буде задіяно, якщо використано середню кнопку миші."

#: ../../reference_manual/preferences/general_settings.rst:175
msgid "On Touch Drag"
msgstr "Сенсорне перетягування"

#: ../../reference_manual/preferences/general_settings.rst:176
msgid "Will activate if it can recognize a touch event. May not always work."
msgstr ""
"Активується, якщо буде розпізнано подію торкання сенсорного екрана. Працює "
"не завжди."

#: ../../reference_manual/preferences/general_settings.rst:178
msgid "Activation"
msgstr "Активація"

#: ../../reference_manual/preferences/general_settings.rst:178
msgid "On Click Drag"
msgstr "Перетягування з клацанням"

#: ../../reference_manual/preferences/general_settings.rst:178
msgid "Will activate when it can recognize a click event, will always work."
msgstr "Активується, якщо буде розпізнано подію клацання. Працює завжди."

#: ../../reference_manual/preferences/general_settings.rst:180
msgid "Sensitivity"
msgstr "Чутливість"

#: ../../reference_manual/preferences/general_settings.rst:181
msgid ""
"How quickly the feature activates, this effective determines the length of "
"the drag."
msgstr ""
"Наскільки швидко активується можливість. Визначає мінімальну довжину "
"перетягування на сенсорному екрані."

#: ../../reference_manual/preferences/general_settings.rst:183
msgid "Kinetic Scrolling (Needs Restart)"
msgstr "Кінетичне гортання (потребує перезапуску програми)"

#: ../../reference_manual/preferences/general_settings.rst:183
msgid "Hide Scrollbar"
msgstr "Сховати панель гортання"

#: ../../reference_manual/preferences/general_settings.rst:183
msgid "Whether to show scrollbars when doing this."
msgstr "Визначає, чи слід показувати смужки гортання."

#: ../../reference_manual/preferences/general_settings.rst:188
msgid "File Handling"
msgstr "Обробка файлів"

#: ../../reference_manual/preferences/general_settings.rst:192
msgid "Enable Autosaving"
msgstr "Увімкнути автозбереження"

#: ../../reference_manual/preferences/general_settings.rst:193
msgid "Determines whether or not Krita should periodically autosave."
msgstr ""
"Визначає, чи слід Krita виконувати періодичне автоматичне створення "
"резервних копій."

#: ../../reference_manual/preferences/general_settings.rst:194
msgid "Autosave Every"
msgstr "Автозбереження кожні"

#: ../../reference_manual/preferences/general_settings.rst:195
msgid ""
"Here the user can specify how often Krita should autosave the file, you can "
"tick the checkbox to turn it off. For Windows these files are saved in the "
"%TEMP% directory. If you are on Linux it is stored in /home/'username'."
msgstr ""
"За допомогою цього пункту користувач може вибрати частоту автоматичного "
"збереження файла у Krita. Також можна позначити пункт вимикання "
"автоматичного збереження. У Windows файли резервних копій зберігатимуться до "
"каталогу %TEMP%. Якщо ви працюєте у Linux, ці файли буде збережено до /"
"home/«ім'я користувача»."

#: ../../reference_manual/preferences/general_settings.rst:196
msgid "Unnamed autosave files are hidden by default"
msgstr "Типово, файли автозбереження файлів без назви приховано"

#: ../../reference_manual/preferences/general_settings.rst:197
msgid ""
"This determines whether the filename of autosaves has a period prepended to "
"the name. On Linux and Mac OS this is a technique to ensure the file is "
"hidden by default."
msgstr ""
"За допомогою цього пункту можна визначити, чи буде додаватися крапка на "
"початку назв автоматично збережених копій файлів. У Linux та Mac OS "
"додавання крапки на початку назви робить файл прихованим."

#: ../../reference_manual/preferences/general_settings.rst:198
msgid "Create Backup File"
msgstr "Створити файл резервної копії"

#: ../../reference_manual/preferences/general_settings.rst:199
msgid ""
"When selected Krita will, upon save, rename the original file as a backup "
"file and save the current image to the original name. The result is that you "
"will have saved the image, and there will be a copy of the image that is "
"saved separately as a backup. This is useful in case of crashes during saves."
msgstr ""
"Якщо буде позначено цей пункт, під час збереження даних Krita "
"перейменовуватиме початковий файл, створюючи його резервну копію, і "
"зберігатиме нову версію зображення до файла із початковою назвою. У "
"результаті ви матимете збережене зображення та копію зображення, яку буде "
"збережено окремо, як резервну. Наявність таких резервних копій є корисною у "
"випадку, якщо трапиться аварійне завершення роботи програми."

#: ../../reference_manual/preferences/general_settings.rst:201
msgid "The default location these backups should be stored."
msgstr "Типове місце, до якого слід зберігати ці резервні копії."

#: ../../reference_manual/preferences/general_settings.rst:203
msgid "Same Folder as Original File"
msgstr "Та сама тека, що і початковий файл"

#: ../../reference_manual/preferences/general_settings.rst:204
msgid "Store the file in the same folder as the original file was stored."
msgstr "Зберігати файл у тій самій теці, що і початковий файл."

#: ../../reference_manual/preferences/general_settings.rst:205
msgid "User Folder"
msgstr "Тека користувача"

#: ../../reference_manual/preferences/general_settings.rst:206
msgid ""
"This is the main folder of your computer. On Linux and Mac OS this is the "
"'Home' folder, on Windows, the 'c:\\Users\\YOUR_USER_NAME' folder (where "
"YOUR_USER_NAME is your windows username)."
msgstr ""
"Це головна тека на вашому комп'ютері. У Linux та Mac OS це тека «Домівка», а "
"у Windows — тека «c:\\Users\\ВАШЕ_ІМ'Я_КОРИСТУВАЧА» (де "
"ВАШЕ_ІМ'Я_КОРИСТУВАЧА — ім'я вашого користувача у Windows)."

#: ../../reference_manual/preferences/general_settings.rst:208
msgid "Backup File Location"
msgstr "Розташування файла резервної копії"

#: ../../reference_manual/preferences/general_settings.rst:208
msgid "Temporary File Folder"
msgstr "Тека для тимчасових файлів"

#: ../../reference_manual/preferences/general_settings.rst:208
msgid ""
"This stored the file in the temp folder. Temp folders are special folders of "
"which the contents are emptied when you shut down your computer. If you "
"don't particularly care about your backup files and want them to be "
"'cleaned' automatically, this is the best place. If you want your backup "
"files to be kept indefinitely, this is a wrong choice."
msgstr ""
"Цей пункт визначає теку для зберігання тимчасових файлів. Теки для "
"тимчасових файлів є спеціалізованими теками, вміст яких вилучається, коли ви "
"вимикаєте операційну систему. Якщо ви не переймаєтеся долею тимчасових "
"файлів і хочете, щоб система вилучала їх автоматично, це саме той пункт. "
"Якщо ж, ви хочете, щоб файли резервних копій зберігалися, доки ви самі їх не "
"вилучите, не використовуйте цей пункт."

#: ../../reference_manual/preferences/general_settings.rst:210
msgid "Backup File Suffix"
msgstr "Суфікс назви резервної копії"

#: ../../reference_manual/preferences/general_settings.rst:211
msgid ""
"The suffix that will be placed after the full filename. 'filename.kra' will "
"then be saved as 'filename.kra~', ensuring the files won't show up in "
"Krita's open file dialog."
msgstr ""
"Суфікс назви, який буде використано після повної назви файла. 'файл.kra' "
"буде збережено як 'файл.kra~', щоб усунути файли резервних копій із "
"діалогового вікна відкриття файлів Krita."

#: ../../reference_manual/preferences/general_settings.rst:212
msgid "Number of Backup Files Kept"
msgstr "Кількість резервних копій для зберігання"

#: ../../reference_manual/preferences/general_settings.rst:213
msgid ""
"Number of backup files Krita keeps, by default this is only one, but this "
"can be up to 99. Krita will then number the backup files."
msgstr ""
"Кількість файлів резервних копій, які має зберігати Krita. Типово, програма "
"зберігає лише одну копію, але ви можете вибрати будь-яку кількість, аж до "
"99. Krita пронумерує файли резервних копій відповідним чином."

#: ../../reference_manual/preferences/general_settings.rst:214
msgid "Compress \\*.kra files more."
msgstr "Стискати файли \\*.kra."

#: ../../reference_manual/preferences/general_settings.rst:215
msgid ""
"This increases the zip compression on the saved Krita files, which makes "
"them lighter on disk, but this takes longer to load."
msgstr ""
"Збільшує ступінь стискання zip для збережених файлів Krita, отже зменшує "
"розмір файлів на диску, але збільшує тривалість завантаження цих файлів."

#: ../../reference_manual/preferences/general_settings.rst:217
msgid "Kra files are zip files. Zip64 allows you to use"
msgstr "Файли kra є архівами zip. Zip64 надає вам змогу скористатися"

#: ../../reference_manual/preferences/general_settings.rst:218
msgid "Use Zip64"
msgstr "Використовувати Zip64"

#: ../../reference_manual/preferences/general_settings.rst:223
msgid "Miscellaneous"
msgstr "Різне"

#: ../../reference_manual/preferences/general_settings.rst:226
msgid ""
"This is the option for handling user sessions. It has the following options:"
msgstr ""
"Цю вкладку пов'язано із роботою сеансів користувача. Тут розміщено такі "
"пункти:"

#: ../../reference_manual/preferences/general_settings.rst:228
msgid "Open Default Window"
msgstr "Відкрити типове вікно"

#: ../../reference_manual/preferences/general_settings.rst:229
msgid "This opens the regular empty window with the last used workspace."
msgstr ""
"Відкривати звичайне порожнє вікно із останнім використаним робочим простором."

#: ../../reference_manual/preferences/general_settings.rst:230
msgid "Load Previous Session"
msgstr "Завантажити попередній сеанс"

#: ../../reference_manual/preferences/general_settings.rst:231
msgid ""
"Load the last opened session. If you have :guilabel:`Save session when Krita "
"closes` toggled, this becomes the last files you had open and the like."
msgstr ""
"Завантажити останній відкритий сеанс. Якщо позначено пункт :guilabel:"
"`Зберігати сеанс при закритті Krita`, буде відкрито останні файли, над якими "
"ви працювали та відтворено інші параметри роботи."

#: ../../reference_manual/preferences/general_settings.rst:233
msgid "Show Session Manager"
msgstr "Показати керування сеансами"

#: ../../reference_manual/preferences/general_settings.rst:233
msgid "Show the session manager directly so you can pick a session."
msgstr ""
"Показати засіб керування сеансами безпосередньо, щоб ви могли вибрати сеанс."

#: ../../reference_manual/preferences/general_settings.rst:234
msgid "When Krita starts"
msgstr "Під час запуску Krita"

#: ../../reference_manual/preferences/general_settings.rst:237
msgid ""
"Save the current open windows, documents and the like into the current "
"session when closing Krita so you can resume where you left off."
msgstr ""
"Зберегти дані щодо поточних відкритих вікон, документів та інші параметри до "
"поточного сеансу при закритті вікна Krita, щоб потім можна було відновити "
"роботу з того самого місця, на якому її було завершено."

#: ../../reference_manual/preferences/general_settings.rst:238
msgid "Save session when Krita closes"
msgstr "Зберігати сеанс під час закриття Krita"

#: ../../reference_manual/preferences/general_settings.rst:240
msgid "Upon importing Images as Layers, convert to the image color space."
msgstr ""
"Під час імпортування зображень як шарів перетворювати простір кольорів "
"зображення."

#: ../../reference_manual/preferences/general_settings.rst:241
msgid ""
"This makes sure that layers are the same color space as the image, necessary "
"for saving to PSD."
msgstr ""
"Це забезпечує використання для шарів того самого простору кольорів, що і для "
"зображення. Необхідно для збереження до формату PSD."

#: ../../reference_manual/preferences/general_settings.rst:242
msgid "Undo Stack Size"
msgstr "Розмір буфера скасування"

#: ../../reference_manual/preferences/general_settings.rst:243
msgid ""
"This is the number of undo commands Krita remembers. You can set the value "
"to 0 for unlimited undos."
msgstr ""
"Це кількість команд журналу скасовування-повернення дій, які "
"запам'ятовуватиме Krita. Встановіть значення 0, якщо програма має "
"запам'ятовувати усі дії."

#: ../../reference_manual/preferences/general_settings.rst:244
msgid "Favorite Presets"
msgstr "Улюблені набори"

#: ../../reference_manual/preferences/general_settings.rst:245
msgid ""
"This determines the amount of presets that can be used in the pop-up palette."
msgstr ""
"Визначає кількість наборів пензлів, якими можна користуватися за допомогою "
"контекстної палітри."

#: ../../reference_manual/preferences/general_settings.rst:247
msgid ""
"This'll hide the splash screen automatically once Krita is fully loaded."
msgstr ""
"Автоматично приховати екран вітання повністю, щойно завантаження Krita буде "
"завершено."

#: ../../reference_manual/preferences/general_settings.rst:251
msgid "Hide splash screen on startup."
msgstr "Ховати вікно вітання під час запуску."

#: ../../reference_manual/preferences/general_settings.rst:251
msgid ""
"Deprecated because Krita now has a welcome widget when no canvas is open."
msgstr ""
"Вважається застарілим, оскільки у Krita тепер є вітальний віджет, якщо у "
"програмі не відкрито полотна."

#: ../../reference_manual/preferences/general_settings.rst:253
msgid "Enable Native File Dialog"
msgstr "Увімкнути природне вікно роботи з файлами"

#: ../../reference_manual/preferences/general_settings.rst:254
msgid ""
"This allows you to use the system file dialog. By default turned off because "
"we cannot seem to get native file dialogues 100% bugfree."
msgstr ""
"Надає змогу використовувати природне для системи діалогове вікно роботи з "
"файлами. Типово вимкнено, оскільки, здається, повністю усунути вади, "
"пов'язані із такими вікнами, неможливо."

#: ../../reference_manual/preferences/general_settings.rst:255
msgid "Maximum brush size"
msgstr "Макс. розмір пензля"

#: ../../reference_manual/preferences/general_settings.rst:256
msgid ""
"This allows you to set the maximum brush size to a size of up to 10.000 "
"pixels. Do be careful with using this, as a 10.000 pixel size can very "
"quickly be a full gigabyte of data being manipulated, per dab. In other "
"words, this might be slow."
msgstr ""
"За допомогою цього пункту можна встановити максимальний розмір пензля у "
"значення, яке не перевищує 10000 пікселів. Слід бути обережним із "
"визначенням розміру пензля, оскільки розмір у 10000 пікселів може призвести "
"до потреби у роботі з цілим гігабайтом пам'яті для кожного мазка. Іншими "
"словами, використання пензлів великих розмірів може значно уповільнити "
"роботу програми."

#: ../../reference_manual/preferences/general_settings.rst:258
msgid "Krita will recalculate the cache when you're not doing anything."
msgstr "Krita оновлюватиме кеш, коли ви не виконуватимете ніяких дій."

#: ../../reference_manual/preferences/general_settings.rst:262
msgid ""
"This is now in the :ref:`performance_settings` under :guilabel:`Animation "
"Cache`."
msgstr ""
"Цей пункт у нових версіях переміщено до параметрів :ref:"
"`performance_settings` і названо :guilabel:`Кеш анімації`."

#~ msgid "Disabled"
#~ msgstr "Вимкнено"

#~ msgid "Will never activated."
#~ msgstr "Ніколи не активується."

#~ msgid ""
#~ "When selected Krita will try to save a backup file in case of a crash."
#~ msgstr ""
#~ "Якщо позначено, Krita намагатиметься зберегти файл резервної копії, якщо "
#~ "станеться аварійне завершення роботи програми."
