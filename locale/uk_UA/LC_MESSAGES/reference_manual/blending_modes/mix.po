# Translation of docs_krita_org_reference_manual___blending_modes___mix.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___blending_modes___mix\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:35+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/blending_modes/mix.rst:None
msgid ".. image:: images/blending_modes/mix/Greaterblendmode.gif"
msgstr ".. image:: images/blending_modes/mix/Greaterblendmode.gif"

#: ../../reference_manual/blending_modes/mix.rst:1
msgid ""
"Page about the mix blending modes in Krita: Allanon, Alpha Darken, Behind, "
"Erase, Geometric Mean, Grain Extract, Grain Merge, Greater, Hard Mix, Hard "
"Overlay, Interpolation, Interpolation2x, Normal, Overlay, Parallel, Penumbra "
"A, B, C and D."
msgstr ""
"Цей розділ присвячено режимам змішування шарів у Krita: Алланон, Альфа-"
"затемнення, Задній план, Стерти, Середнє геометричне, Видобування зерен, "
"Об'єднання зерен, Більше, Агресивне змішування, Жорстке накладання, "
"Інтерполяція, Інтерполяція — 2X, Звичайний, Накладання, Паралель, Півтінь A, "
"B, C, та D."

#: ../../reference_manual/blending_modes/mix.rst:16
msgid "Mix"
msgstr "Змішування"

#: ../../reference_manual/blending_modes/mix.rst:18
#: ../../reference_manual/blending_modes/mix.rst:22
msgid "Allanon"
msgstr "Алланон"

#: ../../reference_manual/blending_modes/mix.rst:24
msgid ""
"Blends the upper layer as half-transparent with the lower. (It add the two "
"layers together and then halves the value)."
msgstr ""
"Змішує верхній шар, як напівпрозорий, із нижнім (два шари додаються між "
"собою, а результат ділиться навпіл)."

#: ../../reference_manual/blending_modes/mix.rst:29
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Allanon_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Allanon_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:29
msgid "Left: **Normal**. Right: **Allanon**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Алланон**."

#: ../../reference_manual/blending_modes/mix.rst:31
#: ../../reference_manual/blending_modes/mix.rst:35
msgid "Interpolation"
msgstr "Інтерполяція"

#: ../../reference_manual/blending_modes/mix.rst:37
msgid ""
"Subtract 0.5f by 1/4 of cosine of base layer subtracted by 1/4 of cosine of "
"blend layer assuming 0-1 range. The result is similar to Allanon mode, but "
"with more contrast and functional difference to 50% opacity."
msgstr ""
"Від 0.5f віднімається 1/4 косинуса значення у шарі основи, а потім від "
"результату віднімається 1/4 косинуса значення у шарі змішування. Результатом "
"має бути число у діапазоні від 0 до 1. Результат застосування подібний до "
"режиму Алланона, але з більшою контрастністю та функціональною різницею до "
"50% непрозорості."

#: ../../reference_manual/blending_modes/mix.rst:43
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Interpolation_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Interpolation_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:43
msgid "Left: **Normal**. Right: **Interpolation**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Інтерполяція**."

#: ../../reference_manual/blending_modes/mix.rst:45
msgid "Interpolation2x"
msgstr "Інтерполяція — 2X"

#: ../../reference_manual/blending_modes/mix.rst:49
msgid "Interpolation - 2X"
msgstr "Інтерполяція — 2X"

#: ../../reference_manual/blending_modes/mix.rst:51
msgid ""
"Applies Interpolation blend mode to base and blend layers, then duplicate to "
"repeat interpolation blending."
msgstr ""
"Застосовує режим змішування «Інтерполяція» до шару основи і змішування, "
"потім подвоює результат для повторного застосування змішування інтерполяцією."

#: ../../reference_manual/blending_modes/mix.rst:56
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Interpolation_X2_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Interpolation_X2_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:56
msgid "Left: **Normal**. Right: **Interpolation - 2X**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Інтерполяція — 2X**."

#: ../../reference_manual/blending_modes/mix.rst:58
#: ../../reference_manual/blending_modes/mix.rst:62
msgid "Alpha Darken"
msgstr "Альфа-затемнення"

#: ../../reference_manual/blending_modes/mix.rst:64
msgid ""
"As far as I can tell this seems to premultiply the alpha, as is common in "
"some file-formats."
msgstr ""
"Попереднє множення каналу прозорості, поширене у деяких форматах файлів."

#: ../../reference_manual/blending_modes/mix.rst:69
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Alpha_Darken_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Alpha_Darken_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:69
msgid "Left: **Normal**. Right: **Alpha Darken**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Альфа-затемнення**."

#: ../../reference_manual/blending_modes/mix.rst:71
#: ../../reference_manual/blending_modes/mix.rst:75
msgid "Behind"
msgstr "Задній план"

#: ../../reference_manual/blending_modes/mix.rst:77
msgid ""
"Does the opposite of normal, and tries to have the upper layer rendered "
"below the lower layer."
msgstr ""
"Протилежність до звичайного змішування: верхній шар розміщується під нижнім "
"шаром."

#: ../../reference_manual/blending_modes/mix.rst:82
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Behind_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Behind_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:82
msgid "Left: **Normal**. Right: **Behind**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Задній план**."

#: ../../reference_manual/blending_modes/mix.rst:84
msgid "Erase (Blending Mode)"
msgstr "Стерти (режим змішування)"

#: ../../reference_manual/blending_modes/mix.rst:88
msgid "Erase"
msgstr "Стерти"

#: ../../reference_manual/blending_modes/mix.rst:90
msgid ""
"This subtracts the opaque pixels of the upper layer from the lower layer, "
"effectively erasing."
msgstr ""
"Віднімає непрозорі пікселі верхнього шару від нижнього шару, фактично, "
"виконуючи витирання."

#: ../../reference_manual/blending_modes/mix.rst:95
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Erase_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Erase_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:95
msgid "Left: **Normal**. Right: **Erase**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Стерти**."

#: ../../reference_manual/blending_modes/mix.rst:97
#: ../../reference_manual/blending_modes/mix.rst:101
msgid "Geometric Mean"
msgstr "Середнє геометричне"

#: ../../reference_manual/blending_modes/mix.rst:103
msgid ""
"This blending mode multiplies the top layer with the bottom, and then "
"outputs the square root of that."
msgstr ""
"У цьому режимі змішування програма множить верхній шар із нижнім, а потім "
"виводить квадратний корінь із отриманого добутку."

#: ../../reference_manual/blending_modes/mix.rst:108
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Geometric_Mean_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Geometric_Mean_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:108
msgid "Left: **Normal**. Right: **Geometric Mean**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Середнє геометричне**."

#: ../../reference_manual/blending_modes/mix.rst:110
#: ../../reference_manual/blending_modes/mix.rst:114
msgid "Grain Extract"
msgstr "Видобування зерен"

#: ../../reference_manual/blending_modes/mix.rst:116
msgid ""
"Similar to subtract, the colors of the upper layer are subtracted from the "
"colors of the lower layer, and then 50% gray is added."
msgstr ""
"Подібний до віднімання: кольори верхнього шару віднімаються від кольорів "
"нижнього шару, а потім додається 50% сірого."

#: ../../reference_manual/blending_modes/mix.rst:121
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Grain_Extract_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Grain_Extract_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:121
msgid "Left: **Normal**. Right: **Grain Extract**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Видобування зерен**."

#: ../../reference_manual/blending_modes/mix.rst:123
#: ../../reference_manual/blending_modes/mix.rst:127
msgid "Grain Merge"
msgstr "Об’єднання зерен"

#: ../../reference_manual/blending_modes/mix.rst:129
msgid ""
"Similar to addition, the colors of the upper layer are added to the colors, "
"and then 50% gray is subtracted."
msgstr ""
"Подібний до додавання: кольори верхнього шару додаються до кольорів нижнього "
"шару, а потім віднімається 50% сірого."

#: ../../reference_manual/blending_modes/mix.rst:134
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Grain_Merge_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Grain_Merge_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:134
msgid "Left: **Normal**. Right: **Grain Merge**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Об'єднання зерен**."

#: ../../reference_manual/blending_modes/mix.rst:136
msgid "Greater (Blending Mode)"
msgstr "Більше (режим змішування)"

#: ../../reference_manual/blending_modes/mix.rst:140
msgid "Greater"
msgstr "Більше"

#: ../../reference_manual/blending_modes/mix.rst:142
msgid ""
"A blending mode which checks whether the painted color is painted with a "
"higher opacity than the existing colors. If so, it paints over them, if not, "
"it doesn't paint at all."
msgstr ""
"Режим змішування, у якому програма визначає, чи є непрозорість намальованого "
"кольору вищою за непрозорість наявних кольорів. Якщо це так, програма "
"вибирає намальований колір, а якщо ні — взагалі не враховує намальоване "
"згори."

#: ../../reference_manual/blending_modes/mix.rst:147
#: ../../reference_manual/blending_modes/mix.rst:151
msgid "Hard Mix"
msgstr "Агресивне змішування"

#: ../../reference_manual/blending_modes/mix.rst:153
msgid "Similar to Overlay."
msgstr "Подібний до накладання."

#: ../../reference_manual/blending_modes/mix.rst:155
msgid ""
"Mixes both Color Dodge and Burn blending modes. If the color of the upper "
"layer is darker than 50%, the blending mode will be Burn, if not the "
"blending mode will be Color Dodge."
msgstr ""
"Поєднує режими висвітлювання кольорів та вигоряння. Якщо колір верхнього "
"шару є темнішим за 50%, режимом змішування буде «вигоряння», а якщо ні — "
"«висвітлювання кольорів»."

#: ../../reference_manual/blending_modes/mix.rst:161
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Hard_Mix_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Hard_Mix_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:161
msgid "Left: **Normal**. Right: **Hard Mix**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Жорстке змішування**."

#: ../../reference_manual/blending_modes/mix.rst:166
msgid "Hard Mix (Photoshop)"
msgstr "Жорстке змішування (Photoshop)"

#: ../../reference_manual/blending_modes/mix.rst:168
msgid "This is the hard mix blending mode as it is implemented in photoshop."
msgstr ""
"Це режим агресивного змішування у формі, у якій його реалізовано у Photoshop."

#: ../../reference_manual/blending_modes/mix.rst:174
msgid ".. image:: images/blending_modes/mix/Krita_4_0_hard_mix_ps.png"
msgstr ".. image:: images/blending_modes/mix/Krita_4_0_hard_mix_ps.png"

#: ../../reference_manual/blending_modes/mix.rst:174
msgid ""
"**Left**: Dots are mixed in with the normal blending mode, on the **Right**: "
"Dots are mixed in with hardmix."
msgstr ""
"**Ліворуч**: крапки змішано у звичайному режимі. **Праворуч**: крапки "
"змішано у режимі агресивного змішування."

#: ../../reference_manual/blending_modes/mix.rst:176
msgid ""
"This add the two values, and then checks if the value is above the maximum. "
"If so it will output the maximum, otherwise the minimum."
msgstr ""
"У цьому режимі програма додає значення шарів і перевіряє, чи не перевищено "
"максимальне можливе значення. Якщо значення перевищено, буде виведено "
"максимальне можливе значення, якщо ні — мінімальне можливе значення."

#: ../../reference_manual/blending_modes/mix.rst:178
msgid "Hard OVerlay"
msgstr "Жорстке накладання"

#: ../../reference_manual/blending_modes/mix.rst:182
msgid "Hard Overlay"
msgstr "Жорстке накладання"

#: ../../reference_manual/blending_modes/mix.rst:186
msgid ""
"Similar to Hard light but hard light use Screen when the value is above 50%. "
"Divide gives better results than Screen, especially on floating point images."
msgstr ""
"Подібний до режиму «яскраве світло», але для «яскравого світла» "
"використовується «екран», якщо значення перевищує 50%. Ділення дає кращі "
"результати за «Екран», особливо для зображень із дійсними (нецілими) "
"значеннями компонентів кольорів."

#: ../../reference_manual/blending_modes/mix.rst:191
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Hard_Overlay_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Hard_Overlay_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:191
msgid "Left: **Normal**. Right: **Hard Overlay**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Жорстке накладання**."

#: ../../reference_manual/blending_modes/mix.rst:193
msgid "Normal (Blending Mode)"
msgstr "Звичайний (режим змішування)"

#: ../../reference_manual/blending_modes/mix.rst:193
msgid "Source Over"
msgstr "Джерело згори"

#: ../../reference_manual/blending_modes/mix.rst:197
msgid "Normal"
msgstr "Звичайний"

#: ../../reference_manual/blending_modes/mix.rst:199
msgid ""
"As you may have guessed this is the default Blending mode for all layers."
msgstr ""
"Як можна здогадатися з назви, це типовий режим змішування для усіх шарів."

#: ../../reference_manual/blending_modes/mix.rst:201
msgid ""
"In this mode, the computer checks on the upper layer how transparent a pixel "
"is, which color it is, and then mixes the color of the upper layer with the "
"lower layer proportional to the transparency."
msgstr ""
"У цьому режимі програма перевіряє, наскільки прозорим є піксель у верхньому "
"шарі, якого він кольору, а потім змішує колір верхнього шару із кольором "
"нижнього пропорційно до рівня прозорості."

#: ../../reference_manual/blending_modes/mix.rst:206
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Normal_50_Opacity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Normal_50_Opacity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:206
msgid "Left: **Normal** 100% Opacity. Right: **Normal** 50% Opacity."
msgstr ""
"Ліворуч: **звичайне змішування**б 100% непрозорості. Праворуч: **Звичайне "
"змішування**, 50% непрозорості."

#: ../../reference_manual/blending_modes/mix.rst:208
msgid "Overlay (Blending Mode)"
msgstr "Накладання (режим змішування)"

#: ../../reference_manual/blending_modes/mix.rst:212
msgid "Overlay"
msgstr "Накладання"

#: ../../reference_manual/blending_modes/mix.rst:214
msgid ""
"A combination of the Multiply and Screen blending modes, switching between "
"both at a middle-lightness."
msgstr ""
"Поєднання режимі змішування «Множення» та «Екран» із перемиканням між "
"режимами на середньому значенні освітленості."

#: ../../reference_manual/blending_modes/mix.rst:216
msgid ""
"Overlay checks if the color on the upperlayer has a lightness above 0.5. If "
"so, the pixel is blended like in Screen mode, if not the pixel is blended "
"like in Multiply mode."
msgstr ""
"У режимі накладки програма перевіряє, чи колір верхнього шару має "
"освітленість, що перевищує 0.5. Якщо це так, змішування відбувається у "
"режимі «Екран», якщо ні — змішування відбувається у режимі «Множення»."

#: ../../reference_manual/blending_modes/mix.rst:218
msgid "This is useful for deepening shadows and highlights."
msgstr "Корисний для поглиблення тіней та виблисків."

#: ../../reference_manual/blending_modes/mix.rst:223
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Overlay_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Overlay_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:223
msgid "Left: **Normal**. Right: **Overlay**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Накладання**."

#: ../../reference_manual/blending_modes/mix.rst:225
#: ../../reference_manual/blending_modes/mix.rst:229
msgid "Parallel"
msgstr "Паралель"

#: ../../reference_manual/blending_modes/mix.rst:231
msgid ""
"This one first takes the percentage in two decimal behind the comma for both "
"layers. It then adds the two values. Divides 2 by the sum."
msgstr ""
"У цьому режимі програма додає пікселі шарів у пропорції, де частки "
"визначаються із точністю до двох знаків після крапки. Результат отримується "
"діленням 2 на суму."

#: ../../reference_manual/blending_modes/mix.rst:238
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Parallel_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Parallel_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:238
msgid "Left: **Normal**. Right: **Parallel**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Паралель**."

#: ../../reference_manual/blending_modes/mix.rst:240
#: ../../reference_manual/blending_modes/mix.rst:244
msgid "Penumbra A"
msgstr "Півтінь A"

#: ../../reference_manual/blending_modes/mix.rst:246
msgid ""
"Creates a linear penumbra falloff. This means most tones will be in the "
"midtone ranges."
msgstr ""
"Створює лінійне спадання півтіні. Це означає, що більшість тонів потраплять "
"у діапазон середніх тонів."

#: ../../reference_manual/blending_modes/mix.rst:251
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Penumbra_A_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Penumbra_A_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:251
msgid "Left: **Normal**. Right: **Penumbra A**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Півтінь A**."

#: ../../reference_manual/blending_modes/mix.rst:253
#: ../../reference_manual/blending_modes/mix.rst:257
msgid "Penumbra B"
msgstr "Півтінь B"

#: ../../reference_manual/blending_modes/mix.rst:259
msgid "Penumbra A with source and destination layer swapped."
msgstr "«Півтінь A», у якій поміняли місцями джерело і призначення."

#: ../../reference_manual/blending_modes/mix.rst:264
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Penumbra_B_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Penumbra_B_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:264
msgid "Left: **Normal**. Right: **Penumbra B**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Півтінь B**."

#: ../../reference_manual/blending_modes/mix.rst:266
#: ../../reference_manual/blending_modes/mix.rst:270
msgid "Penumbra C"
msgstr "Півтінь C"

#: ../../reference_manual/blending_modes/mix.rst:272
msgid ""
"Creates a penumbra-like falloff using arc-tangent formula. This means most "
"tones will be in the midtone ranges."
msgstr ""
"Створює спадання, подібне до півтіні з використанням формули з арктангенсом. "
"Це означає, що більшість тонів потраплять у діапазон середніх тонів."

#: ../../reference_manual/blending_modes/mix.rst:277
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Penumbra_C_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Penumbra_C_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:277
msgid "Left: **Normal**. Right: **Penumbra C**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Півтінь C**."

#: ../../reference_manual/blending_modes/mix.rst:279
#: ../../reference_manual/blending_modes/mix.rst:283
msgid "Penumbra D"
msgstr "Півтінь D"

#: ../../reference_manual/blending_modes/mix.rst:285
msgid "Penumbra C with source and destination layer swapped."
msgstr "«Півтінь C», у якій поміняли місцями джерело і призначення."

#: ../../reference_manual/blending_modes/mix.rst:290
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Penumbra_D_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Penumbra_D_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/mix.rst:290
msgid "Left: **Normal**. Right: **Penumbra D**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Півтінь D**."
