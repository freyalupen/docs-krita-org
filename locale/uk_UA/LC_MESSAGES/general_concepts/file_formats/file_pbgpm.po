# Translation of docs_krita_org_general_concepts___file_formats___file_pbgpm.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___file_formats___file_pbgpm\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 09:36+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../<generated>:1
msgid ".ppm"
msgstr ".ppm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:1
msgid "The PBM, PGM and PPM file formats as exported by Krita."
msgstr "Формати файлів PBM, PGM і PPM у експортуванні за допомогою Krita."

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "*.pbm"
msgstr "*.pbm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "*.pgm"
msgstr "*.pgm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "*.ppm"
msgstr "*.ppm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "PBM"
msgstr "PBM"

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "PGM"
msgstr "PGM"

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "PPM"
msgstr "PPM"

#: ../../general_concepts/file_formats/file_pbgpm.rst:17
msgid "\\*.pbm, \\*.pgm and \\*.ppm"
msgstr "\\*.pbm, \\*.pgm і \\*.ppm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:18
msgid ""
"``.pbm``, ``.pgm`` and ``.ppm`` are a series of file-formats with a similar "
"logic to them. They are designed to save images in a way that the result can "
"be read as an ASCII file, from back when email clients couldn't read images "
"reliably."
msgstr ""
"``.pbm``, ``.pgm`` та ``.ppm`` — серія форматів файлів зі схожою базовою "
"логікою. Ці формати розроблено для зберігання зображень у такий спосіб, щоб "
"результатом був текстовий файл із символами ASCII. Формат було розроблено у "
"часи, коли клієнтські програми для роботи із електронною поштою не могли ще "
"читати дані зображень належним чином."

#: ../../general_concepts/file_formats/file_pbgpm.rst:20
msgid ""
"They are very old file formats, and not used outside of very specialized "
"usecases, such as embedding images inside code."
msgstr ""
"Це дуже старі формати файлів, їхнє використання дуже спеціалізоване. "
"Зокрема, ці формати файлів використовуються для безпосереднього вбудовування "
"зображень до програмного коду."

#: ../../general_concepts/file_formats/file_pbgpm.rst:22
msgid ".pbm"
msgstr ".pbm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:23
msgid "One-bit and can only show strict black and white."
msgstr ""
"Однобітовий, може бути використано для показу лише чорного і білого кольорів."

#: ../../general_concepts/file_formats/file_pbgpm.rst:24
msgid ".pgm"
msgstr ".pgm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:25
msgid "Can show 255 values of gray (8bit)."
msgstr "Можна використовувати 255 відтінків сірого (8-бітовий)."

#: ../../general_concepts/file_formats/file_pbgpm.rst:27
msgid "Can show 8bit rgb values."
msgstr "Можна використовувати 8-бітові кольори RGB."
