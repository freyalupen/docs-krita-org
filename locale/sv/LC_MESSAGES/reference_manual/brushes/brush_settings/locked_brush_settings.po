# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-02 09:38+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "Unlock (Keep Locked)"
msgstr "Lås upp (behåll låst)"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: högerknapp"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:1
msgid "How to keep brush settings locked in Krita."
msgstr "Hur man håller penselinställningar låsta i Krita."

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:11
#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:16
msgid "Locked Brush Settings"
msgstr "Låsta penselinställningar"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:19
msgid ""
"Normally, a changing to a different brush preset will change all brush "
"settings. Locked presets are a way for you to prevent Krita from changing "
"all settings. So, if you want to have the texture be that same over all "
"brushes, you lock the texture parameter. That way, all brush-preset you "
"select will now share the same texture!"
msgstr ""
"Normalt ändras alla penselinställningar när man byter till en annan "
"penselförinställning. Låsta förinställningar är ett sätt att förhindra Krita "
"från att ändra alla inställningar. Om man alltså vill att strukturen ska "
"vara likadan för alla penslar, låser man strukturparametern. På så sätt "
"delar alla penselförinställningar som man väljer samma struktur."

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:22
msgid "Locking a brush parameter"
msgstr "Låsa en penselparameter"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:25
msgid ".. image:: images/brushes/Krita_2_9_brushengine_locking_01.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_locking_01.png"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:26
msgid ""
"To lock an option, |mouseright| the little lock icon next to the parameter "
"name, and set it to :guilabel:`Lock`. It will now be highlighted to show "
"it's locked:"
msgstr ""
"Högerklicka på den lilla låsikonen intill parameternamnet och ändra den "
"till :guilabel:`Lås` för att låsa ett alternativ. Den markeras nu för att "
"visa att den är lås:"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:29
msgid ".. image:: images/brushes/Krita_2_9_brushengine_locking_02.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_locking_02.png"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:30
msgid "And on the canvas, it will show that the texture-option is locked."
msgstr "Och på duken visar det att strukturalternativet är låst."

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:33
msgid ".. image:: images/brushes/Krita_2_9_brushengine_locking_04.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_locking_04.png"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:35
msgid "Unlocking a brush parameter"
msgstr "Låsa upp en penselparameter"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:37
msgid "To *unlock*, |mouseright| the icon again."
msgstr "För att *låsa upp* högerklicka på ikonen igen."

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:40
msgid ".. image:: images/brushes/Krita_2_9_brushengine_locking_03.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_locking_03.png"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:41
msgid "There will be two options:"
msgstr "Det finns två alternativ:"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:43
msgid "Unlock (Drop Locked)"
msgstr "Lås upp (kasta låsta)"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:44
msgid ""
"This will get rid of the settings of the locked parameter and take that of "
"the active brush preset. So if your brush had no texture on, using this "
"option will revert it to having no texture."
msgstr ""
"Det gör sig av med inställningen av den låsta parametern och tar den i den "
"aktiva penselförinställningen. Om penseln alltså inte hade någon struktur "
"aktiverad, återställer det här alternativ den till att inte ha någon "
"struktur."

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:46
msgid "This will keep the settings of the parameter even though it's unlocked."
msgstr "Behåller parameters inställningar även om den är upplåst."
