# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:51+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "Palette to use for naming the layers"
msgstr "Palett att använda för namngivning av lager"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: vänsterknapp"

#: ../../reference_manual/split_layer.rst:1
msgid "The Split Layer functionality in Krita"
msgstr "Funktionen Dela lager i Krita"

#: ../../reference_manual/split_layer.rst:10
msgid "Splitting"
msgstr "Delning"

#: ../../reference_manual/split_layer.rst:10
msgid "Color Islands"
msgstr "Färgöar"

#: ../../reference_manual/split_layer.rst:15
msgid "Split Layer"
msgstr "Dela lager"

#: ../../reference_manual/split_layer.rst:16
msgid ""
"Splits a layer according to color. This is useful in combination with the :"
"ref:`colorize_mask` and the :kbd:`R +` |mouseleft| shortcut to select layers "
"at the cursor."
msgstr ""
"Delar ett lager enligt färg. Det är användbart i kombination med  :ref:"
"`colorize_mask` och genvägen :kbd:`R +` vänster musklick för att välja lager "
"vid markören."

#: ../../reference_manual/split_layer.rst:18
msgid "Put all new layers in a group layer"
msgstr "Lägg alla nya lager i ett grupplager"

#: ../../reference_manual/split_layer.rst:19
msgid "Put all the result layers into a new group."
msgstr "Lägg alla resulterande lager i en ny grupp."

#: ../../reference_manual/split_layer.rst:20
msgid "Put every layer in its own, separate group layer"
msgstr "Lägg alla lager i sina egna, separata, grupplager."

#: ../../reference_manual/split_layer.rst:21
msgid "Put each layer into its own group."
msgstr "Lägg varje lager i sin egen grupp."

#: ../../reference_manual/split_layer.rst:22
msgid "Alpha-lock every new layer"
msgstr "Alfa-lås alla nya lager"

#: ../../reference_manual/split_layer.rst:23
msgid "Enable the alpha-lock for each layer so it is easier to color."
msgstr "Aktivera alfa-lås för varje lager så att det är enklare att färglägga."

#: ../../reference_manual/split_layer.rst:24
msgid "Hide the original layer"
msgstr "Dölj originallager"

#: ../../reference_manual/split_layer.rst:25
msgid ""
"Turns off the visibility on the original layer so you won't get confused."
msgstr "Stänger av originallagrets synlighet så att man inte blir förvirrad."

#: ../../reference_manual/split_layer.rst:26
msgid "Sort layers by amount of non-transparent pixels"
msgstr "Sortera lager enligt mängden ogenomskinliga bildpunkter"

#: ../../reference_manual/split_layer.rst:27
msgid ""
"This ensures that the layers with large color swathes will end up at the top."
msgstr "Säkerställer att lagren med stora färgremsor hamnar överst."

#: ../../reference_manual/split_layer.rst:28
msgid "Disregard opacity"
msgstr "Ta inte hänsyn till ogenomskinlighet"

#: ../../reference_manual/split_layer.rst:29
msgid "Whether to take into account transparency of a color."
msgstr "Om en färgs ogenomskinlighet ska tas hänsyn till."

#: ../../reference_manual/split_layer.rst:30
msgid "Fuzziness"
msgstr "Suddighet"

#: ../../reference_manual/split_layer.rst:31
msgid ""
"How precise the algorithm should be. The larger the fuzziness, the less "
"precise the algorithm will be. This is necessary for splitting layers with "
"anti-aliasing, because otherwise you would end up with a separate layer for "
"each tiny pixel."
msgstr ""
"Hur precis algoritmen ska vara. Ju större suddighet, desto mindre precis "
"blir algoritmen. Det är nödvändigt för att dela lager med kantutjämning, "
"eftersom annars skulle man få ett separat lager för varje liten bildpunkt."

#: ../../reference_manual/split_layer.rst:33
msgid ""
"Select the palette to use for naming. Krita will compare the main color of a "
"layer to each color in the palette to get the most appropriate name for it."
msgstr ""
"Välj paletten som ska användas för namngivning. Krita jämför ett lagers "
"huvudfärg med varje färg i paletten för att få fram det lämpligaste namnet "
"för det."
