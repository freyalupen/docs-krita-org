# Vincent Pinon <vpinon@kde.org>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-03-12 00:06+0100\n"
"Last-Translator: Vincent Pinon <vpinon@kde.org>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 2.0\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../general_concepts/file_formats/file_gbr.rst:1
msgid "The Gimp Brush file format as used in Krita."
msgstr ""

#: ../../general_concepts/file_formats/file_gbr.rst:10
msgid "Gimp Brush"
msgstr ""

#: ../../general_concepts/file_formats/file_gbr.rst:10
msgid "GBR"
msgstr ""

#: ../../general_concepts/file_formats/file_gbr.rst:10
msgid "*.gbr"
msgstr ""

#: ../../general_concepts/file_formats/file_gbr.rst:15
msgid "\\*.gbr"
msgstr ""

#: ../../general_concepts/file_formats/file_gbr.rst:17
msgid ""
"The GIMP brush format. Krita can open, save and use these files as :ref:"
"`predefined brushes <predefined_brush_tip>`."
msgstr ""

#: ../../general_concepts/file_formats/file_gbr.rst:19
msgid ""
"There's three things that you can decide upon when exporting a ``*.gbr``:"
msgstr ""

#: ../../general_concepts/file_formats/file_gbr.rst:21
msgid "Name"
msgstr "Nom"

#: ../../general_concepts/file_formats/file_gbr.rst:22
msgid ""
"This name is different from the file name, and will be shown inside Krita as "
"the name of the brush."
msgstr ""

#: ../../general_concepts/file_formats/file_gbr.rst:23
msgid "Spacing"
msgstr "Espacement"

#: ../../general_concepts/file_formats/file_gbr.rst:24
msgid "This sets the default spacing."
msgstr ""

#: ../../general_concepts/file_formats/file_gbr.rst:26
msgid "Use color as mask"
msgstr "Utiliser la couleur comme masque"

#: ../../general_concepts/file_formats/file_gbr.rst:26
msgid ""
"This'll turn the darkest values of the image as the ones that paint, and the "
"whitest as transparent. Untick this if you are using colored images for the "
"brush."
msgstr ""

#: ../../general_concepts/file_formats/file_gbr.rst:28
msgid ""
"GBR brushes are otherwise unremarkable, and limited to 8bit color precision."
msgstr ""
